package main

import (
	"encoding/json"
	"fmt"
	"math"
	"math/rand"
	"net/http"
	"os"
)

const (
	emptyTodos       = "Todo list is empty"
	redirectInformer = "Please access todos api from /todos end point"
	todoDoesNotExist = "Todo doesn't exist"
)

type Todo struct {
	Id     int    `json:"id"`
	Task   string `json:"task"`
	Status bool   `json:"completed"`
}

type Info struct {
	Message string `json:"message"`
}

var todos []Todo

func getTodos(w http.ResponseWriter, r *http.Request) {
	if len(todos) == 0 {
		fmt.Fprint(w, handleMarshal(Info{Message: emptyTodos}))
		return
	}

	fmt.Fprint(w, handleMarshal(todos))
}

func postTodos(w http.ResponseWriter, r *http.Request) {
	todo := Todo{}
	todo.Id = rand.Intn(math.MaxInt64)
	err := json.NewDecoder(r.Body).Decode(&todo)
	handleError(err)

	todos = append(todos, todo)
	getTodos(w, r) // Return all todos after a new todo
}

func putTodos(w http.ResponseWriter, r *http.Request) {
	todo := Todo{}
	err := json.NewDecoder(r.Body).Decode(&todo)
	handleError(err)

	todoExist := false
	for todoIndex, currentTodo := range todos {
		if todo.Id == currentTodo.Id {
			todos = append(todos[:todoIndex], todos[todoIndex+1:]...) // Delete that matching todo
			todos = append(todos, todo)                               // Update with new details
			todoExist = true
			break
		}
	}

	if !todoExist { // Display message if todo to update doesn't exist
		fmt.Fprint(w, handleMarshal(Info{Message: todoDoesNotExist}))
		return
	}

	getTodos(w, r) // Return all todos after updating a todo
}

func deleteTodos(w http.ResponseWriter, r *http.Request) {
	todo := Todo{}
	err := json.NewDecoder(r.Body).Decode(&todo)
	handleError(err)

	todoExist := false
	for todoIndex, currentTodo := range todos {
		if todo.Id == currentTodo.Id {
			todos = append(todos[:todoIndex], todos[todoIndex+1:]...)
			todoExist = true
			break
		}
	}

	if !todoExist {
		fmt.Fprint(w, handleMarshal(Info{Message: todoDoesNotExist}))
		return
	}

	getTodos(w, r) // Return all todos after deleting a todo
}

func handleRoot(w http.ResponseWriter, r *http.Request) {
	info := Info{
		Message: redirectInformer,
	}
	fmt.Fprint(w, handleMarshal(info))
}

func handleMethods(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		getTodos(w, r)
	case http.MethodPost:
		postTodos(w, r)
	case http.MethodPut:
		putTodos(w, r)
	case http.MethodDelete:
		deleteTodos(w, r)
	}
}

func handleMarshal(i interface{}) string {
	jsonFormat, err := json.MarshalIndent(i, "", "   ")
	handleError(err)

	return string(jsonFormat)
}

func handleError(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

func main() {
	http.HandleFunc("/", handleRoot)
	http.HandleFunc("/todos", handleMethods)
	port := os.Getenv("PORT")
	http.ListenAndServe(":"+port, nil)
}
