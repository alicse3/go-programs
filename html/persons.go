package main

import (
	"fmt"
	"net/http"
	"os"
	"text/template"
)

type Person struct {
	Name      string
	Age       int
	Developer bool
}

var persons [3]Person

func handler(w http.ResponseWriter, r *http.Request) {
	persons[0] = Person{
		Name:      "Ali",
		Age:       26,
		Developer: true,
	}
	persons[1] = Person{
		Name:      "Cute Puppy",
		Age:       3,
		Developer: false,
	}
	persons[2] = Person{
		Name:      "UK",
		Age:       25,
		Developer: true,
	}

	wd, err := os.Getwd()
	handlerError(err)

	tmplt, err := template.ParseFiles(wd + "/html/displayer.html")
	handlerError(err)

	err = tmplt.Execute(w, persons)
	handlerError(err)
}

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8000", nil)
}

func handlerError(err error) {
	if err != nil {
		fmt.Println(err)
	}
}
