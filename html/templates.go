package main

import (
	"fmt"
	"html/template"
	"net/http"
	"os"
)

var names = []string{"Ali", "Puppy", "Uk"}
var ages = []int{26, 3, 25}

var tpl *template.Template

func init() {
	wd, err := os.Getwd()
	errorHandler(err)

	tpl = template.Must(template.ParseGlob(wd + "/html/templates/*.html"))
}

func namesHandler(w http.ResponseWriter, r *http.Request) {
	err := tpl.ExecuteTemplate(w, "names.html", names)
	errorHandler(err)
}

func agesHandler(w http.ResponseWriter, r *http.Request) {
	err := tpl.ExecuteTemplate(w, "ages.html", ages)
	errorHandler(err)
}

func errorHandler(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

func main() {
	http.HandleFunc("/names", namesHandler)
	http.HandleFunc("/ages", agesHandler)
	http.ListenAndServe(":8000", nil)
}
