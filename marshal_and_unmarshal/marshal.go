package main

import (
	"encoding/json"
	"fmt"
)

type Book struct {
	Title  string `json:"title"`
	Author Author `json:"author"`
}

type Author struct {
	Name  string `json:"name"`
	Age   int    `json:"age"`
	Codes bool   `json:"he_codes"`
}

func main() {

	author := Author{
		Name:  "Ali",
		Age:   26,
		Codes: true,
	}

	book := Book{
		Title:  "Go code... Become Go Pro...",
		Author: author,
	}

	byteArray, err := json.MarshalIndent(book, "", "   ")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(string(byteArray))
}
