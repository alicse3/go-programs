package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Todos struct {
	UserID    int    `json:"userId"`
	ID        int    `json:"id"`
	Title     string `json:"title"`
	Completed bool   `json:"completed"`
}

func main() {
	response, err := http.Get("https://jsonplaceholder.typicode.com/todos")
	if err != nil {
		fmt.Println(err)
	}
	defer response.Body.Close()

	byteBodyArray, err := ioutil.ReadAll(response.Body)
	if err != nil {
		fmt.Println(err)
	}

	var todos []Todos
	json.Unmarshal(byteBodyArray, &todos)
	fmt.Println("Valid json :", json.Valid(byteBodyArray))
	for _, v := range todos {
		fmt.Println(v.ID, v.UserID, v.Completed, v.Title)
	}
}
