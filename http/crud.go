package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

const API_URL = "https://jsonplaceholder.typicode.com/"

func getPosts() {
	response, err := http.Get(API_URL + "posts")
	handlerError(err)

	data, err := ioutil.ReadAll(response.Body)
	handlerError(err)

	fmt.Println(string(data))

}

func getPost(postId int) {
	response, err := http.Get(API_URL + "posts/" + strconv.Itoa(postId))
	handlerError(err)

	data, err := ioutil.ReadAll(response.Body)
	fmt.Println(string(data))
}

func getComments(postId int) {
	response, err := http.Get(API_URL + "posts/" + strconv.Itoa(postId) + "/comments")
	handlerError(err)

	data, err := ioutil.ReadAll(response.Body)
	fmt.Println(string(data))
}

type Post struct {
	UserId int    `json:"userId"`
	Id     int    `json:"id"`
	Title  string `json:"title"`
	Body   string `json:"body"`
}

func addPost(p Post) {
	jsonFormat, err := json.Marshal(p)
	handlerError(err)

	client := &http.Client{}
	response, err := client.Post(API_URL+"posts", "application/json", bytes.NewBuffer(jsonFormat))
	handlerError(err)

	data, err := ioutil.ReadAll(response.Body)
	handlerError(err)

	fmt.Println(string(data))
}

func putPost(p Post) {
	jsonFormat, err := json.MarshalIndent(p, "", "   ")
	handlerError(err)

	request, err := http.NewRequest(http.MethodPut, API_URL+"posts", bytes.NewBuffer(jsonFormat))
	handlerError(err)

	data, err := ioutil.ReadAll(request.Body)
	handlerError(err)

	fmt.Println(string(data))
}

func deletePost(postId int) {
	request, err := http.NewRequest(http.MethodDelete, API_URL+"posts/"+strconv.Itoa(postId), nil)
	handlerError(err)

	client := &http.Client{}
	response, err := client.Do(request)
	handlerError(err)

	data, err := ioutil.ReadAll(response.Body)
	handlerError(err)
	fmt.Println(string(data))
}

func handlerError(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

func main() {
	getPosts()
	getPost(100)
	getComments(100)

	post := Post{
		UserId: 101,
		Id:     101,
		Title:  "Hello World!",
		Body:   "Learning Go!",
	}
	addPost(post)

	uPost := Post{
		UserId: 101,
		Id:     101,
		Title:  "Hello Go!",
		Body:   "I'm Going... 😇",
	}
	putPost(uPost)

	deletePost(101) // the resource will not be really deleted on the server but it will be faked as if.
}
