package main

import (
	"fmt"
	"net/http"
)

func defaultHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hello World!")
}

func aliHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hello Ali!")
}

func main() {
	http.HandleFunc("/", defaultHandler)
	http.HandleFunc("/ali", aliHandler)
	http.ListenAndServe(":8000", nil)
}
