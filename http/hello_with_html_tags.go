package main

import (
	"fmt"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "<h1>Hello World!</h1>")
	fmt.Fprint(w, `
	<b>That's interesting to see...</b>
	<br>
	<i>This is displayed from backtick block</i>
	`)
}

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8000", nil)
}
