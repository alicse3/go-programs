package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		query := r.URL.Query()
		fmt.Println(query)
		fmt.Println(query.Get("name"))

		fmt.Fprintln(w, "Hello World!")
	})

	log.Fatal(http.ListenAndServe(":8000", nil))
}
