package main

import "fmt"

func main() {
	var arrayLength, rotationCount int
	fmt.Scanf("%d %d", &arrayLength, &rotationCount)

	array := make([]int, arrayLength)
	for i := 0; i < arrayLength; i++ {
		fmt.Scanf("%d", &array[i])
	}

	lastIndex := rotationCount % arrayLength
	printArray(array[lastIndex:])
	printArray(array[:lastIndex])
}

func printArray(result []int) {
	for _, v := range result {
		fmt.Printf("%d ", v)
	}
}
