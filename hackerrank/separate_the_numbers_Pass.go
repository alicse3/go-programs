package main

import (
	"fmt"
	"strconv"
)

func main() {
	var num int
	fmt.Scanf("%d", &num)

	var numString string
	for i := 0; i < num; i++ {
		fmt.Scanf("%s", &numString)
		result := false
		for j := 1; j <= len(numString)/2; j++ {
			chars := numString[:j]
			newString := chars
			for k := 1; len(newString) < len(numString); k++ {
				newString = newString + getString(getNumber(chars)+k)
			}
			if numString == newString {
				result = true
				fmt.Println("YES", chars)
				break
			}
		}
		if !result {
			fmt.Println("NO")
		}
	}
}

func getString(str int) string {
	return strconv.Itoa(str)
}

func getNumber(str string) int {
	num, err := strconv.Atoi(str)
	checkError(err)
	return num
}

func checkError(err error) {
	if err != nil {
		fmt.Errorf("%s", err)
	}
}
