package main

import "fmt"

func main() {
	var n, k, q, qind int
	fmt.Scanf("%d %d %d", &n, &k, &q)

	array := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &array[i])
	}

	// If rotation count is greater than n
	k = k % n
	startingIndex := n - k

	for i := 0; i < q; i++ {
		fmt.Scanf("%d", &qind)
		fmt.Println(array[(startingIndex+qind)%n])
	}
}
