package main

import (
	"fmt"
	"strconv"
	"strings"
)

var firstString, secondString string
var firstNum, secondNum int

func main() {
	var queries int
	fmt.Scanf("%d", &queries)

	var numberString string
	for i := 0; i < queries; i++ {
		fmt.Scanf("%s", &numberString)
		result, resultNum := solve(numberString)
		if result {
			fmt.Println("YES", resultNum)
		} else {
			fmt.Println("NO")
		}
	}
}

func solve(numberString string) (bool, string) {
	result := false
	resultNum := ""
	fstart, fend, sstart, send, firstNumTracker := 0, 1, 1, 2, 0
	for i := 1; i < len(numberString); i++ {
		firstString = numberString[fstart:fend]
		if send > len(numberString) {
			break
		}
		secondString = numberString[sstart:send]
		fmt.Println(i, firstString, secondString)
		if nextNum(firstString, secondString) {
			if firstNumTracker == 0 {
				resultNum = firstString
			}
			result = true
			fstart = sstart
			fend = send
			sstart += len(secondString)
			send += len(secondString)
			firstNumTracker++
		} else {
			result = false
			if len(secondString) > len(firstString) {
				firstNumTracker = 0
				fstart = 0
				fend++
				sstart++
			} else if len(secondString) == len(firstString) {

			}
			send++
		}
	}
	return result, resultNum
}

func nextNum(f, s string) bool {
	if strings.HasPrefix(f, "0") || strings.HasPrefix(s, "0") {
		return false
	}
	firstNum, _ = strconv.Atoi(f)
	secondNum, _ = strconv.Atoi(s)
	return firstNum+1 == secondNum
}

func getNum(str string) int {
	num, err := strconv.Atoi(str)
	if err != nil {
		return 0
	}
	return num
}

// Failing for 101102103 - logic is wrong

/*
Input:
------
10
00000000000000000000000000000000
11111111111111111111111111111111
22222222222222222222222222222222
33333333333333333333333333333333
44444444444444444444444444444444
55555555555555555555555555555555
66666666666666666666666666666666
77777777777777777777777777777777
88888888888888888888888888888888
10001001100210031004100510061007

Output:
-------
NO
NO
NO
NO
NO
NO
NO
NO
NO
YES 1000
---------------------------------
Input:
------
10
90071992547409929007199254740993
45035996273704964503599627370497
22517998136852482251799813685249
11258999068426241125899906842625
562949953421312562949953421313
90071992547409928007199254740993
45035996273704963503599627370497
22517998136852481251799813685249
11258999068426240125899906842625
562949953421312462949953421313

Output:
-------
YES 9007199254740992
YES 4503599627370496
YES 2251799813685248
YES 1125899906842624
YES 562949953421312
NO
NO
NO
NO
NO
*/
