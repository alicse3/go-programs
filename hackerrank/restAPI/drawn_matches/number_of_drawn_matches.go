package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"sync"
)

// To hold match info
type Match struct {
	Competition string `json:"competition"`
	Year        int    `json:"year"`
	Round       string `json:"round"`
	Team1       string `json:"team1"`
	Team2       string `json:"team2"`
	Team1Goals  string `json:"team1goals"`
	Team2Goals  string `json:"team2goals"`
}

// To hold response info
type ResponseInfo struct {
	Page       string  `json:"page"`
	PerPage    int     `json:"per_page"`
	Total      int     `json:"total"`
	TotalPages int     `json:"total_pages"`
	Data       []Match `json:"data"`
}

// Matches end point
const matches = "https://jsonmock.hackerrank.com/api/football_matches"

// Wait group to wait for a go routine to be completed
var wg sync.WaitGroup

// For getting the matches info
func getInfo(year, pageNumber string) (rInfo ResponseInfo) {
	// Construct request URL
	reqURL := matches + "?year=" + year + "&page=" + pageNumber

	// Send Get request
	resp, err := http.Get(reqURL)
	checkError(err)

	// Close the response body
	defer resp.Body.Close()

	// Read body as bytes
	matchesInBytes, err := ioutil.ReadAll(resp.Body)
	checkError(err)

	// Unmarshal into ResponseInfo type struct
	err = json.Unmarshal(matchesInBytes, &rInfo)
	checkError(err)

	fmt.Println(rInfo)
	return // Named return
}

func main() {
	// Take an year input from user
	var year int
	fmt.Scanf("%d", &year)

	// Run consecutively
	consecutive(year)

	// Run concurrently
	concurrent(year)
}

func concurrent(year int) {
	// Fist request to know the response information
	info := getInfo(getString(year), getString(1))

	// To store result
	result := 0

	// Iterate over all pages
	for i := 1; i <= info.TotalPages; i++ {
		wg.Add(1)
		// Go routine to get data concurrently
		go func(pageNumber int) {
			defer wg.Done()

			// Get response data & Check for team goals match
			for _, goals := range getInfo(getString(year), getString(pageNumber)).Data {
				if goals.Team1Goals == goals.Team2Goals {
					result++
				}
			}
		}(i)
	}
	wg.Wait()

	// Print answer
	fmt.Println(result)
}

func consecutive(year int) {
	// Fist request to know the response information
	info := getInfo(getString(year), getString(1))

	// To store result
	result := 0

	// Iterate over all pages
	for i := 1; i <= info.TotalPages; i++ {
		// Get response data & Check for team goals match
		for _, goals := range getInfo(getString(year), getString(i)).Data {
			if goals.Team1Goals == goals.Team2Goals {
				result++
			}
		}
	}
	wg.Wait()

	// Print answer
	fmt.Println(result)
}

func checkError(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

// Convert and return int to string
func getString(i int) string {
	return strconv.Itoa(i)
}
