package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"sync"
)

// To hold match info
type Match struct {
	Competition string `json:"competition"`
	Year        int    `json:"year"`
	Round       string `json:"round"`
	Team1       string `json:"team1"`
	Team2       string `json:"team2"`
	Team1Goals  string `json:"team1goals"`
	Team2Goals  string `json:"team2goals"`
}

// To hold competition info
type Competition struct {
	Name     string `json:"name"`
	Country  string `json:"country"`
	Year     int    `json:"year"`
	Winner   string `json:"winner"`
	RunnerUp string `json:"runnerup"`
}

// To hold competition page info
type CPageInfo struct {
	Page       int           `json:"page"`
	PerPage    int           `json:"per_page"`
	Total      int           `json:"total"`
	TotalPages int           `json:"total_pages"`
	Data       []Competition `json:"data"`
}

// To hold match page info
type MPageInfo struct {
	Page       string  `json:"page"`
	PerPage    int     `json:"per_page"`
	Total      int     `json:"total"`
	TotalPages int     `json:"total_pages"`
	Data       []Match `json:"data"`
}

// API URLs
const (
	ApiURL = "https://jsonmock.hackerrank.com/api"

	CompetitionsEndPoint = ApiURL + "/football_competitions"
	MatchesEndPoint      = ApiURL + "/football_matches"
)

// Wait group to check & wait until all go routines execution to be completed
var wg = sync.WaitGroup{}

// Get competions info
func getCompetitions(name string, year int) (pInfo CPageInfo) {
	// Construct request URL
	reqURL := CompetitionsEndPoint + "?name=" + url.QueryEscape(name) + "&year=" + getString(year)

	// Initialise client
	client := http.Client{}

	// Send request
	req, err := http.NewRequest(http.MethodGet, reqURL, nil)
	req.Header.Set("Content-Type", "application/json")

	// Get response
	resp, err := client.Do(req)
	checkError(err)

	// Close response body
	defer resp.Body.Close()

	// Read response body
	byteData, err := ioutil.ReadAll(resp.Body)
	checkError(err)

	// Unmarshal and assign it to pInfo named return variable
	err = json.Unmarshal(byteData, &pInfo)
	checkError(err)

	return // Named return
}

func getMatches(query_params string) (pInfo MPageInfo) {
	// Construct request URL
	reqURL := MatchesEndPoint + query_params

	// Get response
	resp, err := http.Get(reqURL)
	checkError(err)

	// Read response body
	byteData, err := ioutil.ReadAll(resp.Body)
	checkError(err)

	// Unmarshal and assign it to pInfo named return variable
	err = json.Unmarshal(byteData, &pInfo)
	checkError(err)

	return // Named return
}

func main() {
	// Input #1:
	// competition_name = "UEFA Champions League"
	// competition_year = 2011
	// Output: 28

	// Input #2:
	// competition_name = "English Premier League"
	// competition_year = 2015
	// Output: 37

	// Take competition name from user
	var competition_name string
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	competition_name = scanner.Text()

	// Take competition year from user
	var competition_year int
	scanner.Scan()
	competition_year = getInt(scanner.Text())

	// Get competitions info - assuming it will be always one record
	cmpts := getCompetitions(competition_name, competition_year)

	// To store an answer
	team1goals, team2goals := 0, 0

	// Iterate over total competition pages
	for i := 1; i <= cmpts.TotalPages; i++ {
		// Go routine to get team1goals goals
		wg.Add(1)
		go func(pageNumber int) {
			defer wg.Done()

			// Get all matches when the winner is team1
			mData := getMatches("?competition=" + url.QueryEscape(competition_name) + "&year=" + getString(competition_year) + "&team1=" + url.QueryEscape(cmpts.Data[0].Winner) + "&page=" + getString(pageNumber)).Data

			// Get Team1Goals
			for _, goals := range mData {
				team1goals += getInt(goals.Team1Goals)
			}
		}(i)

		// Go routine to get team2goals goals
		wg.Add(1)
		go func(pageNumber int) {
			defer wg.Done()

			// Get all matches when the winner is team2
			mData := getMatches("?competition=" + url.QueryEscape(competition_name) + "&year=" + getString(competition_year) + "&team2=" + url.QueryEscape(cmpts.Data[0].Winner) + "&page=" + getString(pageNumber)).Data

			// Get Team2Goals
			for _, goals := range mData {
				team2goals += getInt(goals.Team2Goals)
			}
		}(i)
	}
	wg.Wait()

	// Print an answer
	fmt.Println(team1goals + team2goals)

}

// For checking an error
func checkError(err error) {
	if err != nil {
		fmt.Println(err)
	}
}

// To convert int to string
func getString(i int) string {
	return strconv.Itoa(i)
}

// To convert string to int
func getInt(str string) (i int) {
	i, err := strconv.Atoi(str)
	checkError(err)

	return // Named return
}
