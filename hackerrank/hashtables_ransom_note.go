package main

import "fmt"

func main() {
	var magazineWordsCount, ransomeNoteWordsCount int
	fmt.Scanf("%d %d", &magazineWordsCount, &ransomeNoteWordsCount)

	canCreate := true

	var str string
	magazineWords, ransomeWords := make(map[string]int, magazineWordsCount), make(map[string]int, ransomeNoteWordsCount)
	for i := 0; i < magazineWordsCount; i++ {
		fmt.Scanf("%s", &str)
		magazineWords[str]++
	}
	for i := 0; i < ransomeNoteWordsCount; i++ {
		fmt.Scanf("%s", &str)
		ransomeWords[str]++
	}
	for key, value := range ransomeWords {
		if magazineWords[key] < value {
			canCreate = false
			break
		}
	}
	if canCreate {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}
}
