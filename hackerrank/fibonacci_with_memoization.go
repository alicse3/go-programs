package main

import "fmt"

var fibonacciStore [31]int

func main() {
	var number int
	fmt.Scanf("%d", &number)

	fmt.Println(fibonacci(number))
}

func fibonacci(num int) int {
	if num > 0 && fibonacciStore[num] == 0 {
		if num <= 1 {
			fibonacciStore[num] = num
		} else {
			fibonacciStore[num] = fibonacci(num-1) + fibonacci(num-2)
		}
	}
	return fibonacciStore[num]
}
