package main

import "fmt"

func main() {
	var testCases int
	fmt.Scanf("%d", &testCases)

	var prisoners, sweets, updatedSweets, start, updatedStart int
	for i := 0; i < testCases; i++ {
		fmt.Scanf("%d %d %d", &prisoners, &sweets, &start)

		updatedSweets = sweets % prisoners
		updatedStart = start % prisoners
		fmt.Println(updatedSweets, updatedStart)
		if updatedStart == 0 && updatedStart == 0 {
			fmt.Println(1)
		} else {
			fmt.Println(updatedSweets + updatedStart - 1)
		}
	}
}
