package main

import "fmt"

func main() {
	var num int
	fmt.Scanf("%d", &num)

	fmt.Println(fibonacci(num))
}

func fibonacci(num int) int {
	if num <= 0 {
		return 0
	} else if num == 1 {
		return 1
	}
	return fibonacci(num-1) + fibonacci(num-2)
}
