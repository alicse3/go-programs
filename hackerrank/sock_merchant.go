package main

import "fmt"

func main() {
	var num, input int
	fmt.Scanf("%d", &num)

	countMap := make(map[int]int, num)
	for i := 0; i < num; i++ {
		fmt.Scanf("%d", &input)
		countMap[input]++
	}
	result := 0
	for _, value := range countMap {
		if value >= 2 {
			result += value / 2
		}
	}
	fmt.Println(result)
}
