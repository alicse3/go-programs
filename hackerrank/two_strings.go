package main

import (
	"fmt"
	"strings"
)

const aToz string = "abcdefghijklmnopqrstuvwxyz"

func main() {
	var num int
	fmt.Scanf("%d", &num)

	var contains bool

	var str1, str2 string
	for i := 0; i < num; i++ {
		fmt.Scanf("%s", &str1)
		fmt.Scanf("%s", &str2)

		for j := 0; j < len(aToz); j++ {
			if strings.Contains(str1, string(aToz[j])) && strings.Contains(str2, string(aToz[j])) {
				contains = true
				break
			} else {
				contains = false
			}
		}
		if contains {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}
