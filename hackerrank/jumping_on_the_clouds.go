package main

import (
	"fmt"
	"math"
)

func main() {
	var num int
	fmt.Scanf("%d", &num)

	clouds := make([]int, num)
	zeroCount := 0
	for i := 0; i < num; i++ {
		fmt.Scanf("%d", &clouds[i])
		if clouds[i] == 0 {
			zeroCount++
		}
	}
	if zeroCount == num || zeroCount == 0 {
		fmt.Println(num / 2)
	} else {
		count, result := 0, 0
		for i := 1; i < num; i++ {
			if clouds[i-1] == 0 && clouds[i] == 1 && clouds[i+1] == 0 {
				if i-1 == 0 {
					result += 1
				} else {
					result += getCeil(count) + 1
					count = 0
				}
				i += 1
			} else {
				count++
			}
		}
		fmt.Println(result + getCeil(count))
	}
}

func getCeil(num int) int {
	return int(math.Ceil(float64(num) / 2))
}
