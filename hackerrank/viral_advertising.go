package main

import (
	"fmt"
	"math"
)

func main() {
	var num int
	fmt.Scanf("%d", &num)

	s, b, f := 0, 5, 0
	for i := 0; i < num; i++ {
		f = int(math.Floor(float64(b) / 2))
		s += f
		b = f * 3
	}

	fmt.Println(s)
}
