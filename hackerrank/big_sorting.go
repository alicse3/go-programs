package main

import (
	"fmt"
	"sort"
)

func main() {
	var tcs int
	fmt.Scanf("%d", &tcs)

	unsorted := make([]string, tcs, tcs)
	for i := 0; i < tcs; i++ {
		fmt.Scanf("%s", &unsorted[i])
	}

	sort.SliceStable(unsorted, func(i, j int) bool {
		// If both string lengths aren't equal then check the difference
		// If 1st string length is less than 2nd then return true otherwise return false
		if len(unsorted[i]) != len(unsorted[j]) {
			return (len(unsorted[i]) - len(unsorted[j])) < 0
		}
		// If both strings are equal then compare it's individual digits
		// If digits doesn't match & 1st string digit is greater than 2nd then return true otherwise false
		for k := 0; k < len(unsorted[i]); k++ {
			if unsorted[i][k] != unsorted[j][k] {
				return unsorted[i][k] < unsorted[j][k]
			}
		}
		return true
	})

	fmt.Println(unsorted)
}
