package main

import "fmt"

func main() {
	heights := make(map[string]int, 26)

	var height int
	for i := 0; i < 26; i++ {
		fmt.Scanf("%d", &height)
		heights[string(97+i)] = height
	}

	var word string
	fmt.Scanf("%s", &word)

	max := -1
	for i := 0; i < len(word); i++ {
		max = getMax(max, heights[string(word[i])])
	}
	fmt.Println(max * len(word))
}

func getMax(a, b int) int {
	if a > b {
		return a
	}
	return b
}
