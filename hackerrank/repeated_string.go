package main

import (
	"fmt"
	"strings"
)

func main() {
	var str string
	fmt.Scanf("%s", &str)
	length := len(str)

	var number int
	fmt.Scanf("%d", &number)

	aCount := strings.Count(str, "a")
	reminder := number % length
	quotient := number / length
	if reminder == 0 {
		fmt.Println((aCount * quotient))
	} else {
		extra := strings.Count(str[:reminder], "a")
		fmt.Println((aCount * quotient) + extra)
	}
}
