package main

import (
	"fmt"
	"math"
	"strconv"
	"time"
)

type geometry interface {
	area() float64
	perimeter() float64
}

type circle struct {
	radius float64
}

type rectangle struct {
	width, height float64
}

func (c circle) area() float64 {
	return math.Pi * c.radius * c.radius
}

func (r rectangle) area() float64 {
	return r.width * r.height
}

func (c circle) perimeter() float64 {
	return 2 * math.Pi * c.radius
}

func (r rectangle) perimeter() float64 {
	return 2 * (r.width + r.height)
}

func measure(g geometry) {
	fmt.Println(g)
	fmt.Println(g.area())
	fmt.Println(g.perimeter())
}

func main() {
	fmt.Println("Hello World!")

	crl := circle{
		radius: 10,
	}
	fmt.Println(crl.area())
	fmt.Println(crl.perimeter())

	rec := rectangle{
		height: 10,
		width:  10,
	}
	fmt.Println(rec.area())
	fmt.Println(rec.perimeter())

	fmt.Println("*****")
	measure(crl)
	measure(rec)

	fmt.Println("*****")
	fmt.Println(time.Now())
	fmt.Println(strconv.ParseInt("123", 10, 64))
	fmt.Println(strconv.Atoi("54321"))

	fmt.Println("*****")
	const something int = 1000
	const pi float64 = 3.14
	fmt.Println(something)

	fmt.Println("*****")
	mul := [3][3]int{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
	}
	fmt.Println(mul)
	fmt.Println(mul[0:2])

	var mul2 [][]string
	mul2 = append(mul2, []string{"Ali", "Puppy", "UK"})
	fmt.Println(mul2)

	var mul3 [1][3]int
	mul3[0] = [3]int{3, 6, 9}
	fmt.Println(mul3)

	mul4 := [1][10]float64{}
	fmt.Println(mul4)

	for _, v := range mul {
		for _, w := range v {
			fmt.Print(w, " ")
		}
	}
	fmt.Println()

	first := make([]int, 5)
	for i := range first {
		first[i] = i
	}
	fmt.Println(first)
	second := first
	second[0] = -1
	fmt.Println(first)
	fmt.Println(second)

	third := make([]int, len(first))
	copy(third, first)
	third[0] = 99
	fmt.Println(first)
	fmt.Println(third)
	fmt.Println("*****")

	mulSlice := make([][]int, 3)
	mulSlice[0] = []int{1, 2, 3, 4, 5}
	fmt.Println(mulSlice)
	for _, slice := range mulSlice {
		fmt.Println(slice, len(slice))
	}

	mp := make(map[string]int)
	mp["Ali"] = 777702671449
	fmt.Println(mp, mp["Ali"])

	// nmp := make(map[int]string, 10)
	// var usrNum int
	// fmt.Scanf("%d", &usrNum)
	// for i := 0; i < usrNum; i++ {
	// 	var userInput string
	// 	fmt.Scanf("%s", &userInput)
	// 	nmp[i] = userInput
	// }
	// fmt.Println(nmp, len(nmp))

	a := "97"
	fmt.Println(strconv.Atoi(a))

	ali, isPresent := mp["Ali"]
	fmt.Println(ali, isPresent)

	fmt.Println(add_sub(10, 5))

	fmt.Println(getSum(1, 2, 3, 5, 4))

	greet()("Ali!")
	greet()("Puppy")
	greet()("UK!")

	evens := evenNumbers()
	fmt.Println(evens())
	fmt.Println(evens())
	fmt.Println(evens())

	for i := 0; i < 10; i++ {
		fmt.Print(fib(i), " ")
	}
	fmt.Println()

	for i := 0; i < 10; i++ {
		fmt.Print(i, "=>", fact(i), " \n")
	}

	normal := 3
	fmt.Println(normal)

	ptr := &normal
	fmt.Println("Addrs :", ptr)
	fmt.Println("Value :", *ptr)
	*ptr++
	fmt.Println(normal, *ptr)

	normal++
	fmt.Println(normal, *ptr)
}

func fact(num int) int {
	if num <= 1 {
		return 1
	}
	return num * fact(num-1)
}

func fib(n int) int {
	if n < 1 || n < 2 {
		return 1
	}
	return fib(n-1) + fib(n-2)
}

func evenNumbers() func() int {
	i := 0
	return func() int {
		i += 2
		return i
	}
}

func greet() func(string) {
	return func(name string) {
		fmt.Println("Hello,", name)
	}
}

func add_sub(a, b int) (int, int) {
	return a + b, a - b
}

func getSum(values ...int) int {
	sum := 0
	for _, v := range values {
		sum += v
	}

	func() {
		fmt.Println("Hello! I'm anonymous...")
	}()

	func(sm int) {
		fmt.Println("And the sum from an anonymous func is", sm)
	}(sum)

	return sum
}
