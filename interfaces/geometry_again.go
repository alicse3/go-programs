package main

import (
	"fmt"
	"math"
)

type geometryAgain interface {
	area() float64
	perimeter() float64
}

type rectangleAgain struct {
	width, height float64
}

type circleAgain struct {
	radius float64
}

func (r rectangleAgain) area() float64 {
	return r.width * r.height
}

func (c circleAgain) area() float64 {
	return math.Pi * c.radius * c.radius
}

func (r rectangleAgain) perimeter() float64 {
	return 2 * (r.height + r.width)
}

func (c circleAgain) perimeter() float64 {
	return 2 * math.Pi * c.radius
}

func measureAgain(g geometryAgain) {
	fmt.Println(g.area())
	fmt.Println(g.perimeter())
}

func main() {
	crl := circleAgain{
		radius: 100,
	}
	// fmt.Println(crl.area())
	// fmt.Println(crl.perimeter())

	rec := rectangleAgain{
		height: 100,
		width:  100,
	}
	// fmt.Println(rec.area())
	// fmt.Println(rec.perimeter())

	measureAgain(crl)
	measureAgain(rec)
}
