package greetings

import "bytes"

func GreetMe(name string) string {
	buf := bytes.Buffer{}
	buf.WriteString("Hello ")
	buf.WriteString(name)
	buf.WriteString("! How are you doing?")
	return buf.String()
}
