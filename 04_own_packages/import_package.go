package main

import (
	"alicse3/go_practise/04_own_packages/greetings"
	"fmt"
)

func main() {
	fmt.Println(greetings.GreetMe("Ali"))
}
