package main

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

var wsUpgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

type Message struct {
	Username string `json:"username"`
	Message  string `json:"message"`
}

var clients = make(map[*websocket.Conn]bool)

var broadcast = make(chan Message)

func broadcaster() {
	for {
		message := <-broadcast

		for client := range clients {
			client.WriteJSON(message)
		}
	}
}

func connHandler(w http.ResponseWriter, r *http.Request) {
	wsUpgrader.CheckOrigin = func(r *http.Request) bool { return true }

	conn, err := wsUpgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	clients[conn] = true

	for {
		var message Message

		err = conn.ReadJSON(&message)
		if err != nil {
			log.Fatal(err)
		}

		broadcast <- message
	}
}

func main() {
	go broadcaster()

	http.HandleFunc("/ws", connHandler)
	log.Fatal(http.ListenAndServe(":8000", nil))
}
