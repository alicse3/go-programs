package main

import "fmt"

type Shape struct {
	shapeType string
}

func (s Shape) circle() string {
	return s.shapeType
}

func (s Shape) triangle() string {
	return s.shapeType
}

func (s Shape) rectangle() string {
	return s.shapeType
}

func main() {
	circle := Shape{
		shapeType: "Round",
	}
	fmt.Println(circle.circle())

	triangle := Shape{
		shapeType: "Triangle",
	}
	fmt.Println(triangle.triangle())

	rectangle := Shape{
		shapeType: "Rectangle",
	}
	fmt.Println(rectangle.rectangle())

	ptr_triangle := &Shape{
		shapeType: "Triangle Shape",
	}
	fmt.Println(ptr_triangle)
}
