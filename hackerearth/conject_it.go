package main

import "fmt"

func main() {
	var tcs int
	fmt.Scanf("%d", &tcs)

	var input string
	for ; tcs > 0; tcs-- {
		fmt.Scanf("%s", &input)
		fmt.Println("YES")
	}
}
