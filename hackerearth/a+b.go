package main

import (
	"fmt"
	"math/big"
)

func main() {
	var first, second string

	for true {
		if data, _ := fmt.Scanf("%s %s", &first, &second); data != 0 {
			firstNum, ok := new(big.Int).SetString(first, 10)
			checkOk(ok)
			secondNum, ok := new(big.Int).SetString(second, 10)
			checkOk(ok)
			fmt.Println(firstNum.Add(firstNum, secondNum))
		} else {
			break
		}
	}
}

func checkOk(ok bool) {
	if !ok {
		panic("Error while converting from string to big int")
	}
}
