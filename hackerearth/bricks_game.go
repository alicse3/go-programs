package main

import "fmt"

func main() {
	var num int
	fmt.Scanf("%d", &num)

	// i = Patlu, j = Motu
	i, j, sum := 1, 2, 0
	for ; i <= num; i++ {
		j = i * 2
		// If Patlu/Motu/(Patlu&Motu) sum is greater than num then break
		if (sum+i) >= num || (sum+j) >= num || (sum+i+j) >= num {
			break
		} else {
			sum += i + j
		}
	}
	// Check num is within Patlu range
	if num <= i+sum {
		fmt.Println("Patlu")
	} else {
		fmt.Println("Motu")
	}
}
