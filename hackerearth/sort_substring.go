package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	var t int = getInt(strings.TrimSpace(scanner.Text()))

	var input string
	for ; t > 0; t-- {
		scanner.Scan()
		input = strings.TrimSpace(scanner.Text())
		string_and_nums := strings.Split(strings.TrimSpace(input), " ")
		input = string_and_nums[0]
		start_index, end_index := getInt(string_and_nums[1]), getInt(string_and_nums[2])
		str := strings.Split(input[start_index:end_index+1], "")
		sort.Sort(sort.Reverse(sort.StringSlice(str)))
		fmt.Println(input[:start_index] + strings.Join(str, "") + input[end_index+1:])
	}
}

func getInt(s string) int {
	result, _ := strconv.Atoi(s)
	return result
}
