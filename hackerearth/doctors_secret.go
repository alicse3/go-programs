package main

import "fmt"

func main() {
	var ln, pgs int

	fmt.Scanf("%d %d", &ln, &pgs)

	if ln <= 23 && (pgs >= 500 && pgs <= 1000) {
		fmt.Println("Take Medicine")
	} else {
		fmt.Println("Don't take Medicine")
	}
}
