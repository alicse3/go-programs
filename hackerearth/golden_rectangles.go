package main

import (
	"fmt"
)

func main() {
	var t, c int
	fmt.Scanf("%d", &t)

	var x, y int
	for ; t > 0; t-- {
		fmt.Scanf("%d %d", &x, &y)
		w := float64(x) / float64(y)
		h := float64(y) / float64(x)
		if (w >= 1.6 && w <= 1.7) || (h >= 1.6 && h <= 1.7) {
			c++
		}
	}
	fmt.Println(c)
}
