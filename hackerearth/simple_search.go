package main

import "fmt"

func main() {
	var n, k int
	fmt.Scanf("%d", &n)

	a := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &a[i])
	}

	fmt.Scanf("%d", &k)
	for i := 0; i < n; i++ {
		if k == a[i] {
			fmt.Println(i)
			break
		}
	}
}
