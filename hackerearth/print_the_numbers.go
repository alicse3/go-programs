package main

import "fmt"

func main() {
	var n, v int
	fmt.Scanf("%d", &n)

	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &v)
		fmt.Printf("%d ", v)
	}
}
