package main

import (
	"bufio"
	"fmt"
	"os"
)

var reader *bufio.Reader = bufio.NewReader(os.Stdin)

func scanf(f string, a ...interface{}) { fmt.Fscanf(reader, f, a...) }

func main() {
	var n, p, c, o int
	scanf("%d\n", &n)

	scanf("%d", &p)
	o = 1
	for i := 1; i < n; i++ {
		scanf("%d", &c)
		if p == c {
			continue
		}
		o += 1
		p = c
	}
	fmt.Println(o)
}
