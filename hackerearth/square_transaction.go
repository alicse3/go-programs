package main

import (
	"bufio"
	"fmt"
	"os"
)

var reader *bufio.Reader = bufio.NewReader(os.Stdin)
var writer *bufio.Writer = bufio.NewWriter(os.Stdout)

func printf(f string, a ...interface{}) { fmt.Fprintf(writer, f, a...) }
func scanf(f string, a ...interface{})  { fmt.Fscanf(reader, f, a...) }

func main() {
	var t, q, n int
	scanf("%d\n", &t)

	a := make([]int, t)
	for i := 0; i < t; i++ {
		scanf("%d", &a[i])
	}

	scanf("\n")
	scanf("%d\n", &q)
	for ; q > 0; q-- {
		scanf("%d\n", &n)
		fmt.Println(transactionNumber(a, n))
	}
}

func transactionNumber(a []int, n int) int {
	var sum, ans int = 0, -1
	for i := 0; i < len(a); i++ {
		sum += a[i]
		if sum >= n {
			ans = i
			break
		}
	}
	if ans == -1 {
		return -1
	}
	return ans + 1
}
