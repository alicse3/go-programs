package main

import (
	"fmt"
	"strings"
)

func main() {
	vowels := "aeiou"

	var t, n int
	fmt.Scanf("%d", &t)

	var s string
	for i := 0; i < t; i++ {
		fmt.Scanf("%d", &n)
		fmt.Scanf("%s", &s)

		c := 0
		for j := 0; j < n-1; j++ {
			if !strings.Contains(vowels, string(s[j])) && strings.Contains(vowels, string(s[j+1])) {
				c += 1
			}
		}
		fmt.Println(c)
	}
}
