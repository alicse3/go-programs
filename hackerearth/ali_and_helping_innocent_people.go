package main

import (
	"fmt"
	"strings"
)

func main() {
	vowels := "AEIOUY"

	var s string
	fmt.Scanf("%s", &s)

	if strings.Contains(vowels, string(s[2])) {
		fmt.Println("invalid")
	} else if ((s[0]+s[1])%2 != 0) || ((s[3]+s[4])%2 != 0) || ((s[4]+s[5])%2 != 0) || ((s[7]+s[8])%2 != 0) {
		fmt.Println("invalid")
	} else {
		fmt.Println("valid")
	}
}
