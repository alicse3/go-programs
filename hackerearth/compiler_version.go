package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

const (
	CommentPrefix = "//"
	Old           = "->"
	New           = "."
)

func main() {
	var input string

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		input = scanner.Text()

		code := strings.SplitN(input, CommentPrefix, 2)
		fmt.Println(strings.ReplaceAll(code[0], Old, New) + input[len(code[0]):])
	}
}
