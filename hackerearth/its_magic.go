package main

import (
	"fmt"
)

func main() {
	// Read n
	var n int
	fmt.Scanf("%d", &n)

	// Initialize slice with size n
	a := make([]int, n)

	sum := 0
	// Calculate sum while reading an input
	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &a[i])
		sum += a[i]
	}

	/*
		Iterate over slice and check for a condition
		- exactly divisible by 7
		- first array index of the smallest element
	*/
	min, index := 1000000000, -1
	for k, v := range a {
		if (sum-v)%7 == 0 && min > v {
			min = v
			index = k
		}
	}

	// Print answer
	fmt.Println(index)
}
