package main

import (
	"fmt"
	"sort"
)

func main() {
	var n, sum int
	fmt.Scanf("%d", &n)

	a := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &a[i])
		sum += a[i]
	}

	sort.Ints(a)

	fmt.Println(sum-a[n-1], sum-a[0])
}

// func findMin(a []int) int {
// 	var min = math.MaxInt32
// 	for _, v := range a {
// 		if v < min {
// 			min = v
// 		}
// 	}
// 	return min
// }

// func findMax(a []int) int {
// 	var max = math.MinInt32
// 	for _, v := range a {
// 		if v > max {
// 			max = v
// 		}
// 	}
// 	return max
// }
