package main

import (
	"fmt"
)

func main() {
	var d, c int
	fmt.Scanf("%d", &d)

	// Complete the circle means, have to calculate the perimeter
	var r, x float64
	for ; d > 0; d-- {
		fmt.Scanf("%f %f", &r, &x)
		if (x * 100) >= (2 * 22 * r / 7) {
			c++
		}
	}
	fmt.Println(c)
}
