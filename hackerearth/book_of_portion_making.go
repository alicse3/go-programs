package main

import "fmt"

func main() {
	var isbn string
	fmt.Scanf("%s", &isbn)

	if len(isbn) == 10 && isDivisibleBy11(isbn) {
		fmt.Println("Legal ISBN")
	} else {
		fmt.Println("Illegal ISBN")
	}
}

func isDivisibleBy11(s string) bool {
	sum := 0
	for i := 0; i < len(s); i++ {
		sum += (i + 1) * int(s[i])
	}
	return sum%11 == 0
}
