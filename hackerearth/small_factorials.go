package main

import (
	"fmt"
	"math/big"
)

func main() {
	var tcs int
	fmt.Scanf("%d", &tcs)

	// Store already computed facts
	facts := make(map[string]string)

	var input string
	for ; tcs > 0; tcs-- {
		fmt.Scanf("%s", &input)

		_, ok := facts[input]
		if !ok {
			facts[input] = fact(getBigInt(input))
		}
		fmt.Println(facts[input])
	}
}

func getBigInt(str string) *big.Int {
	bigInt, ok := new(big.Int).SetString(str, 10)
	if !ok {
		fmt.Println("Not Ok!")
	}
	return bigInt
}

func fact(n *big.Int) string {
	result := getBigInt("1")

	for ; n.Cmp(getBigInt("0")) > 0; n.Sub(n, getBigInt("1")) {
		result.Mul(result, n)
	}

	return result.String()
}
