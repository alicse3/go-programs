package main

import "fmt"

func main() {
	var n int
	fmt.Scanf("%d", &n)
	list := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &list[i])
	}
	var q int
	fmt.Scanf("%d", &q)
	for i := 0; i < q; i++ {
		var t, a, b, c int
		fmt.Scanf("%d %d", &t, &a)
		if t == 1 {
			fmt.Scanf("%d %d", &b, &c)
			for j := a - 1; j < b; j++ {
				list[j] += c
			}
		} else if t == 2 {
			fmt.Scanf("%d %d", &b, &c)
			for j := a - 1; j < b; j++ {
				list[j] *= c
			}
		} else {
			first := findIndex(list, a, 0)
			if first != -1 {
				second := findIndex(list, a, -1)
				if second != -1 {
					fmt.Println(((second + 1) - (first + 1)) + 1)
				} else {
					fmt.Println(-1)
				}
			} else {
				fmt.Println(-1)
			}
		}
	}
}

func findIndex(values []int, numberToFind, beginIndex int) int {
	index := -1
	if beginIndex != -1 {
		for i := beginIndex; i < len(values); i++ {
			if values[i] == numberToFind {
				index = i
				break
			}
		}
	} else {
		for i := len(values) - 1; i > 0; i-- {
			if values[i] == numberToFind {
				index = i
				break
			}
		}
	}

	return index
}
