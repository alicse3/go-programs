package main

import (
	"fmt"
)

func main() {
	var t int
	fmt.Scanf("%d", &t)
	for i := 0; i < t; i++ {
		var l, r, k int
		fmt.Scanf("%d %d %d", &l, &r, &k)
		first10Digits := [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
		kMultiples := make([]int, 0, 5)
		for j := 0; j < len(first10Digits); j++ {
			if first10Digits[j]%k == 0 {
				kMultiples = append(kMultiples, first10Digits[j])
			}
		}
		for j := 0; j < len(kMultiples); j++ {
			fmt.Print(kMultiples[j], " ")
		}
		fmt.Println()
		min, max := getMin(kMultiples), getMax(kMultiples)
	Loop:
		for x := 0; x < len(kMultiples); x++ {
			for y := 10; y <= r; y *= 10 {
				if kMultiples[x]*y+min > r {
					break Loop
				}
				fmt.Println("Min : ", kMultiples[x]*y+min)
				fmt.Println("Max : ", kMultiples[x]*y+max)
			}
		}
	}
}

func getMin(values []int) int {
	min := 1000000000000000000
	for _, v := range values {
		if v < min {
			min = v
		}
	}
	return min
}

func getMax(values []int) int {
	max := -1
	for _, v := range values {
		if v > max {
			max = v
		}
	}
	return max
}

// func isSpecial(num, k int) bool {
// 	special := true
// 	for num > 0 {
// 		if (num%10)%k != 0 {
// 			special = false
// 			break
// 		}
// 		num /= 10
// 	}
// 	return special
// }
