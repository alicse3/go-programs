package main

import (
	"bufio"
	"fmt"
	"os"
)

var reader *bufio.Reader = bufio.NewReader(os.Stdin)
var writer *bufio.Writer = bufio.NewWriter(os.Stdout)

func printf(f string, a ...interface{}) { fmt.Fprintf(writer, f, a...) }
func scanf(f string, a ...interface{})  { fmt.Fscanf(reader, f, a...) }

func main() {
	defer writer.Flush()

	var t int
	scanf("%d\n", &t)

	var guy_name, crush_name string
	for ; t > 0; t-- {
		scanf("%s %s\n", &guy_name, &crush_name)
		if accepted(guy_name, crush_name) {
			printf("Love you too\n")
		} else {
			printf("We are only friends\n")
		}
	}
}

func accepted(guy_name, crush_name string) bool {
	var g_index, g_len = 0, len(guy_name)
	for i := 0; i < len(crush_name); i++ {
		if g_index < g_len {
			if string(guy_name[g_index]) == string(crush_name[i]) {
				g_index++
			}
		}
	}
	return g_index == g_len
}
