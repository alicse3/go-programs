package main

import "fmt"

func main() {
	var n, x, y int
	fmt.Scanf("%d", &n)

	fmt.Scanf("%d", &x)
	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &y)
		if y >= x {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}
