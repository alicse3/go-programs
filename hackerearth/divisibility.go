package main

import (
	"fmt"
)

func main() {
	var n, v int
	fmt.Scanf("%d", &n)

	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &v)
	}
	if v%10 == 0 {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}
}
