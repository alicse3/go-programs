package main

import (
	"fmt"
	"strings"
)

func main() {
	var word string
	fmt.Scanf("%s", &word)
	zCount, oCount := strings.Count(word, "z"), strings.Count(word, "o")
	if 2*zCount == oCount {
		fmt.Println("Yes")
	} else {
		fmt.Println("No")
	}
}
