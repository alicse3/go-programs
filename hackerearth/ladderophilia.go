package main

import "fmt"

func main() {
	var steps int
	fmt.Scanf("%d", &steps)

	for i := 1; i <= ((steps*5)+5)+steps; i++ {
		if i%2 != 0 {
			fmt.Print("     ")
		} else if i%6 == 0 {
			fmt.Print("*****")
		} else {
			fmt.Print("*   *")
		}
		fmt.Println()
	}
}
