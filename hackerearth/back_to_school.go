package main

import (
	"fmt"
	"math/big"
)

func main() {
	var tech1, tech2, tech3 string
	fmt.Scanf("%s %s %s", &tech1, &tech2, &tech3)

	number1, ok := new(big.Int).SetString(tech1, 10)
	checkOk(ok)

	number2, ok := new(big.Int).SetString(tech2, 10)
	checkOk(ok)

	number3, ok := new(big.Int).SetString(tech3, 10)
	checkOk(ok)

	if number1.Cmp(number2) > 0 && number1.Cmp(number3) > 0 {
		fmt.Println(number1)
	} else if number2.Cmp(number1) > 0 && number2.Cmp(number3) > 0 {
		fmt.Println(number2)
	} else if number3.Cmp(number1) > 0 && number3.Cmp(number2) > 0 {
		fmt.Println(number3)
	} else {
		fmt.Println(number3)
	}
}

func checkOk(ok bool) {
	if !ok {
		panic("Error while converting to big int")
	}
}
