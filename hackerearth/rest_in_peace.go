package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	var t, n int
	fmt.Scanf("%d", &t)

	var s string
	for ; t > 0; t-- {
		fmt.Scanf("%s", &s)
		n, _ = strconv.Atoi(s)
		if strings.Contains(s, "21") || n%21 == 0 {
			fmt.Println("The streak is broken!")
		} else {
			fmt.Println("The streak lives still in our heart!")
		}
	}
}
