package main

import (
	"fmt"
)

func main() {
	var n, t int
	fmt.Scanf("%d", &n)

	a, b := 0, 7
	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &t)
		if abs(t-a) <= abs(t-b) {
			fmt.Println("A")
			a = t
		} else {
			fmt.Println("B")
			b = t
		}
	}
}

func abs(v int) int {
	if v < 0 {
		return -v
	}
	return v
}
