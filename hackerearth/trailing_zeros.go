package main

import (
	"fmt"
	"math/big"
)

func main() {
	var input string
	fmt.Scanf("%s", &input)

	fmt.Println(getTrailingZerosCount(fact(getBigInt(input))))
}

func getTrailingZerosCount(str string) int {
	count := 0
	for i := len(str) - 1; i > 0; i-- {
		if string(str[i]) != "0" {
			break
		}
		count++
	}
	return count
}

func getBigInt(str string) *big.Int {
	bigInt, ok := new(big.Int).SetString(str, 10)
	if !ok {
		fmt.Println("Not Ok!")
	}
	return bigInt
}

func fact(n *big.Int) string {
	result := getBigInt("1")

	for ; n.Cmp(getBigInt("0")) > 0; n.Sub(n, getBigInt("1")) {
		result.Mul(result, n)
	}

	return result.String()
}
