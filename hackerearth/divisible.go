package main

import (
	"fmt"
	"math/big"
)

func main() {
	// Array of numbers
	var nums int
	fmt.Scanf("%d", &nums)

	var input string
	result := ""
	// Read numbers
	// Get first digit from first half of an array and append it to a string
	// Get last digit from last half of an array and append it to a string
	for i := 1; i <= nums; i++ {
		fmt.Scanf("%s", &input)
		if i <= nums/2 {
			result += input[0:1]
		} else {
			result += input[len(input)-1:]
		}
	}

	// Since a result string may be big - convert and store it into a big integer
	number, ok := new(big.Int).SetString(result, 10)
	if !ok {
		fmt.Println("Error converting result to big int")
	}

	// Convert 11 to big int
	eleven, ok := new(big.Int).SetString("11", 10)
	if !ok {
		fmt.Println("Error converting 11 to big int")
	}

	// Convert 0 to big int
	zero, ok := new(big.Int).SetString("0", 10)
	if !ok {
		fmt.Println("Error converting 0 to big int")
	}

	// Divide number by eleven and compare reminder to be zero
	if new(big.Int).Rem(number, eleven).Cmp(zero) == 0 {
		fmt.Println("OUI")
	} else {
		fmt.Println("NON")
	}
}
