package main

import "fmt"

func main() {
	var t, n int
	fmt.Scanf("%d", &t)

	asterisk, hash := "*", "#"

	for i := 0; i < t; i++ {
		fmt.Scanf("%d", &n)
		for j := 0; j < n; j++ {
			otherSides(j, asterisk)
			for k := j + 1; k < (2*n)-(j+1); k++ {
				fmt.Print(hash)
			}
			otherSides(j, asterisk)
			fmt.Println()
		}
	}
}

func otherSides(j int, asterisk string) {
	for k := 0; k < j+1; k++ {
		fmt.Print(asterisk)
	}
}
