package main

import (
	"fmt"
	"regexp"
	"strconv"
)

func main() {
	var t, suvo_re, suvojit_re int
	fmt.Scanf("%d", &t)

	var name string
	for ; t > 0; t-- {
		fmt.Scanf("%s", &name)
		suvojit_re, suvo_re = len(getRegex("SUVOJIT").FindAllString(name, -1)), len(getRegex("SUVO").FindAllString(getRegex("SUVOJIT").ReplaceAllString(name, "-"), -1))
		fmt.Println("SUVO = " + strconv.Itoa(suvo_re) + ", SUVOJIT = " + strconv.Itoa(suvojit_re))
	}
}

func getRegex(pattern string) *regexp.Regexp {
	return regexp.MustCompile(pattern)
}
