package main

import (
	"fmt"
	"strings"
	"time"
)

func main() {
	var tcs, sh, sm, eh, em int
	fmt.Scanf("%d", &tcs)

	for i := 0; i < tcs; i++ {
		fmt.Scanf("%d %d %d %d", &sh, &sm, &eh, &em)

		diff := time.Date(0, 0, 0, eh, em, 0, 0, time.UTC).Sub(time.Date(0, 0, 0, sh, sm, 0, 0, time.UTC)).String()
		if strings.Contains(diff, "h") {
			spl := strings.Split(diff, "h")
			hrs, mins := spl[0], spl[1]
			fmt.Println(hrs, strings.Split(mins, "m")[0])
		} else {
			fmt.Println(0, strings.Split(diff, "m")[0])
		}
	}
}
