package main

import (
	"fmt"
)

var sticks = map[string]int{
	"0": 6,
	"1": 2,
	"2": 5,
	"3": 5,
	"4": 4,
	"5": 5,
	"6": 6,
	"7": 3,
	"8": 7,
	"9": 6,
}

func main() {
	var tcs, sCount int
	fmt.Scanf("%d", &tcs)

	var numString string
	for ; tcs > 0; tcs-- {
		fmt.Scanf("%s", &numString)

		sCount = getSticksCount(numString)
		if (sCount-3)%2 == 0 {
			printResult(1, 7)
			printResult((sCount-3)/2, 1)
		} else {
			printResult((sCount / 2), 1)
		}
		fmt.Println()
	}
}

func printResult(c, v int) {
	for ; c > 0; c-- {
		fmt.Print(v)
	}
}

func getSticksCount(str string) int {
	count := 0
	for _, v := range str {
		count += sticks[string(v)]
	}
	return count
}
