package main

import "fmt"

func main() {
	var message string
	fmt.Scanf("%s", &message)

	var key rune
	fmt.Scanf("%d", &key)

	for _, v := range message {
		if v >= 65 && v <= 90 {
			fmt.Print(string((((v % 65) + key) % 26) + 65))
		} else if v >= 97 && v <= 122 {
			fmt.Print(string((((v % 97) + key) % 26) + 97))
		} else if v >= 48 && v <= 57 {
			fmt.Print(string((((v % 48) + key) % 10) + 48))
		} else {
			fmt.Print(string(v))
		}
	}
}

/*
Sample inputs and outputs:

abcdZXYzxy-999.@
200
stuvECDrpq-777.@

----------a
100
----------w

All-convoYs-9-be:Alert1.
4
Epp-gsrzsCw-3-fi:Epivx5.

*/
