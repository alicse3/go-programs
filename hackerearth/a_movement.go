package main

import "fmt"

func main() {
	var pos int
	fmt.Scanf("%d", &pos)

	if pos <= 5 {
		fmt.Println(1)
	} else if pos%5 == 0 {
		fmt.Println(pos / 5)
	} else {
		fmt.Println((pos / 5) + 1)
	}
}
