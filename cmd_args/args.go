package main

import (
	"fmt"
	"os"
)

func main() {
	cmdArgs := os.Args
	fmt.Println(cmdArgs)
	fmt.Println(len(cmdArgs))
	for _, v := range cmdArgs {
		fmt.Printf("%s => %T type\n", v, v)
	}
}
