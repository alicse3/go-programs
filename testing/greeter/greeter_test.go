package main

import (
	"testing"
)

func TestGreet(t *testing.T) {
	result := Greet("Ali")
	if result != "Hello Ali!" {
		t.Errorf("Expected 'Hello Ali!' but got %s", result)
	}
}

func TestTableGreet(t *testing.T) {
	tcs := []struct {
		Input  string
		Output string
	}{
		{"Ali", "Hello Ali!"},
		{"Puppy", "Hello Puppy!"},
		{"Uk", "Hello Uk!"},
		{"Nothing", "Hello Nothing!"},
	}
	for _, test := range tcs {
		if Greet(test.Input) != test.Output {
			t.Errorf("Expected %s but got %s", test.Output, Greet(test.Input))
		}
	}
}
