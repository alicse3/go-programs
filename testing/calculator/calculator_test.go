package main

import "testing"

var numbers = []int{1, 2, 3, 4, 5}

func TestSum(t *testing.T) {
	actualSum := Sum(numbers...)
	expectedSum := (len(numbers) * (len(numbers) + 1) / 2)
	if actualSum != expectedSum {
		t.Errorf("Test Failed: %d expected, received: %d", expectedSum, actualSum)
	}
}

func TestSub(t *testing.T) {
	sub := Sub(10, 5)
	if sub != 5 {
		t.Errorf("Expected %d, got %d", 5, sub)
	}
}

func TestMul(t *testing.T) {
	actualMul := Mul(numbers...)
	if actualMul != 120 {
		t.Errorf("Test Failed: %d expected, received: %d", 120, actualMul)
	}
}

func TestDiv(t *testing.T) {
	actualDiv := Div(100, 0)
	if actualDiv != 10 {
		t.Errorf("Test Failed: %d expected, received: %d", 10, actualDiv)
	}
}
