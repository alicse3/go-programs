package main

import "fmt"

func Sum(numbers ...int) int {
	result := 0
	for i := 0; i < len(numbers); i++ {
		result += numbers[i]
	}
	return result
}

func Sub(a, b int) int {
	return a - b
}

func Mul(numbers ...int) int {
	result := 1
	for i := 0; i < len(numbers); i++ {
		result *= numbers[i]
	}
	return result
}

func Div(a, b int) int {
	return a / b
}

func main() {
	fmt.Println(Sum(1, 2))
	fmt.Println(Sub(1, 2))
	fmt.Println(Mul(2, 3))
	fmt.Println(Div(100, 10))
}
