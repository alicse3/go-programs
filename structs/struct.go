package main

import "fmt"

type Person struct {
	name string
	age  int
}

func main() {
	fmt.Println(Person{name: "Ali", age: 26})
	fmt.Println(Person{name: "UK"})
	per1 := Person{
		name: "Ali Mahammad",
		age:  26,
	}
	fmt.Println(per1)
	per2 := Person{
		per1.name,
		per1.age,
	}
	fmt.Println(per2)
	per3 := &per1
	fmt.Println(per3, per3.name, per3.age)
}
