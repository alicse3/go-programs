package main

import (
	"fmt"
)

var outername = "Outer Me"

// anothername := "Sabba" - Can't do outside

func main() {
	var name = "Ali Mahammad"
	fmt.Println("Hello...! Your name is : ", name)

	var nameagain string = "Ali"
	fmt.Println("People calls you : ", nameagain)

	var age int = 24
	fmt.Println("Age is : ", age)

	var areYouHuman = true

	const birthday string = "May 26th, 1995"

	anothername := "Sabba"

	firstName, lastName := "MahammadAli", "S"

	fmt.Printf("%T\n", name)
	fmt.Printf("%T\n", nameagain)
	fmt.Printf("%T\n", age)
	fmt.Printf("%T\n", areYouHuman)
	fmt.Printf("%T\n", birthday)

	fmt.Println(name, nameagain, age, areYouHuman, birthday, outername, anothername, firstName, lastName)
}
