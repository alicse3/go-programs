package main

import (
	"fmt"
	"os"
)

func main() {
	wd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
	}

	data := make([]byte, 1000)
	file, err := os.Open(wd + "/os/fileReading.go")
	if err != nil {
		fmt.Println(err)
	}
	count, err := file.Read(data)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(string(data[:count]))
}
