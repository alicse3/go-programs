package main

import "fmt"

func getSum(a int, b int) int {
	return a + b
}

func reverseAndPrintIt(str string) {
	for i := len(str) - 1; i >= 0; i-- {
		fmt.Printf("%c", str[i])
	}
	fmt.Println()
}

func main() {
	a, b := 3, 5
	fmt.Println("=> The Sum of ", a, "and", b, "is", getSum(a, b))
	reverseAndPrintIt("dammahaM ilA")
}
