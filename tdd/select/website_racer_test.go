package racer

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestWebsiteRacer(t *testing.T) {
	t.Run("should return faster response URL first", func(t *testing.T) {
		slowServer := newDelayServer(3 * time.Millisecond)
		defer slowServer.Close()

		fastServer := newDelayServer(0 * time.Millisecond)
		defer fastServer.Close()

		slowURL := slowServer.URL
		faseURL := fastServer.URL

		got, err := WebsiteRacer(slowURL, faseURL)
		want := faseURL

		if err != nil {
			t.Errorf("Error %v", err)
		}

		if got != want {
			t.Errorf("got %q but want %q", got, want)
		}
	})

	t.Run("should return an error if response takes more than 10 secs", func(t *testing.T) {
		fastServer := newDelayServer(10 * time.Second)
		slowServer := newDelayServer(11 * time.Second)

		_, err := ConfigurableWebsiteRacer(fastServer.URL, slowServer.URL, 1*time.Second)
		if err == nil {
			t.Error("Alright... Returned within 10 secs...")
		}
	})
}

func newDelayServer(delayTime time.Duration) *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(delayTime)
		w.WriteHeader(http.StatusOK)
	}))
}
