package racer

import (
	"fmt"
	"net/http"
	"time"
)

var tenSecTimeout = 10 * time.Second

func WebsiteRacer(x, y string) (string, error) {
	return ConfigurableWebsiteRacer(x, y, tenSecTimeout)
}

func ConfigurableWebsiteRacer(x, y string, timeout time.Duration) (string, error) {
	select {
	case <-ping(x):
		return x, nil
	case <-ping(y):
		return y, nil
	case <-time.After(timeout):
		return "", fmt.Errorf("timeout waiting for %s and %s responses", x, y)
	}
}

func ping(url string) chan struct{} {
	ch := make(chan struct{})
	go func() {
		http.Get(url)
		close(ch)
	}()
	return ch
}
