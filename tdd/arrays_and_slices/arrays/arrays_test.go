package arrays

import "testing"

func TestSum(t *testing.T) {
	t.Run("should return collection of 5 numbers sum", func(t *testing.T) {
		numbers := [5]int{1, 2, 5, 4, 3}

		got := ArraySum(numbers)
		want := 15

		if got != want {
			t.Errorf("got %d but want %d given, %v", got, want, numbers)
		}
	})
}
