package slices

func SliceSum(numbers []int) int {
	sum := 0
	for _, number := range numbers {
		sum += number
	}
	return sum
}

func SumAll(allNumbers ...[]int) (sums []int) {
	for _, num := range allNumbers {
		sums = append(sums, SliceSum(num))
	}
	return
}

func SumAllTails(numbers ...[]int) (sums []int) {
	for _, num := range numbers {
		if len(num) == 0 {
			sums = append(sums, 0)
		} else {
			sums = append(sums, num[len(num)-1])
		}
	}
	return
}
