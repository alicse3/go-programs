package slices

import (
	"reflect"
	"testing"
)

func TestSliceSum(t *testing.T) {
	t.Run("should return collection of any numbers sum", func(t *testing.T) {
		numbers := []int{1, 2, 3}

		got := SliceSum(numbers)
		want := 6

		if got != want {
			t.Errorf("got %d but want %d given, %v", got, want, numbers)
		}
	})
}

func TestSumAll(t *testing.T) {
	got := SumAll([]int{1, 2}, []int{3, 4})
	want := []int{3, 7}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v but want %v", got, want)
	}
}

func TestSumAllTails(t *testing.T) {
	got := SumAllTails(
		[]int{1, 2},
		[]int{3, 4, 5},
		[]int{6, 7, 8},
		[]int{})
	want := []int{2, 5, 8, 0}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %d but want %d", got, want)
	}
}
