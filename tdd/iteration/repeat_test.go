package iterations

import (
	"fmt"
	"testing"
)

func TestRepeat(t *testing.T) {
	got := Repeat("s", 7)
	want := "sssssss"

	if got != want {
		t.Errorf("got %q but want %q", got, want)
	}
}

func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("s", 3)
	}
}

func ExampleRepeat() {
	repeated := Repeat("Ali", 3)
	fmt.Println(repeated)
	// Output: AliAliAli
}
