package main

import (
	"fmt"
	"io"
	"os"
	"time"
)

const (
	finalWord = "Go!"

	startingCountDownNumber = 3
)

type Sleeper interface {
	Sleep()
}

type SpySleeper struct {
	Calls int
}

func (s *SpySleeper) Sleep() {
	s.Calls++
}

type DefaultSleeper struct{}

func (d *DefaultSleeper) Sleep() {
	time.Sleep(1 * time.Second)
}

const (
	SleepCall = "sleep"
	WriteCall = "write"
)

type CountDownSpyPrinter struct {
	Calls []string
}

func (c *CountDownSpyPrinter) Sleep() {
	c.Calls = append(c.Calls, SleepCall)
}

// Named return - i is 0 and error is nil - default values
func (c *CountDownSpyPrinter) Write(b []byte) (i int, err error) {
	c.Calls = append(c.Calls, WriteCall)
	return
}

func CountDown(writer io.Writer, sleeper Sleeper) {
	for i := startingCountDownNumber; i > 0; i-- {
		sleeper.Sleep()
		fmt.Fprintln(writer, i)
	}
	sleeper.Sleep()
	fmt.Fprint(writer, finalWord)
}

func main() {
	defaultSleeper := &DefaultSleeper{}
	CountDown(os.Stdout, defaultSleeper)
}
