package main

import (
	"bytes"
	"reflect"
	"testing"
)

func TestCountDown(t *testing.T) {
	t.Run("count down from 3 to Go!", func(t *testing.T) {
		buffer := &bytes.Buffer{}
		spySleeper := &SpySleeper{}

		CountDown(buffer, spySleeper)

		got := buffer.String()
		want := `3
2
1
Go!`

		if got != want {
			t.Errorf("got %q but want %q", got, want)
		}

		if spySleeper.Calls != 4 {
			t.Errorf("got %d but want %d", spySleeper.Calls, 4)
		}
	})

	t.Run("sleep and write calls", func(t *testing.T) {
		countDownSpyPrinter := &CountDownSpyPrinter{}

		CountDown(countDownSpyPrinter, countDownSpyPrinter)

		got := countDownSpyPrinter.Calls
		want := []string{
			SleepCall,
			WriteCall,
			SleepCall,
			WriteCall,
			SleepCall,
			WriteCall,
			SleepCall,
			WriteCall,
		}

		if !reflect.DeepEqual(got, want) {
			t.Errorf("got %q but want %q", got, want)
		}
	})
}
