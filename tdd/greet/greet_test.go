package main

import "testing"

func TestGreet(t *testing.T) {
	assertResult := func(t *testing.T, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got %q but want %q", got, want)
		}
	}

	t.Run("should say hello in Telugu", func(t *testing.T) {
		got := Greet("You", "Telugu")
		want := "Namaste You!"
		assertResult(t, got, want)
	})

	t.Run("should say 'Hello World!'", func(t *testing.T) {
		got := Greet("", "")
		want := "Hello World!"
		assertResult(t, got, want)
	})

	t.Run("should say hello in Kannada", func(t *testing.T) {
		got := Greet("Puppy", "Kannada")
		want := "Namaskara Puppy!"
		assertResult(t, got, want)
	})
}
