package main

import "fmt"

const (
	telugu  = "Telugu"
	kannada = "Kannada"
)

const (
	helloPrefix     = "Hello "
	namastePrefix   = "Namaste "
	namaskaraPrefix = "Namaskara "

	exclamation = "!"
)

// func greetingPrefix(lang string) string {
// 	switch lang {
// 	case telugu:
// 		return namastePrefix
// 	case kannada:
// 		return namaskaraPrefix
// 	default:
// 		return helloPrefix
// 	}
// }

/*
In our function signature we have made a named return value (prefix string).
This will create a variable called prefix in your function.
It will be assigned the "zero" value. This depends on the type, for example ints are 0 and for strings it is "".
You can return whatever it's set to by just calling return rather than return prefix.
This will display in the Go Doc for your function so it can make the intent of your code clearer.
*/

func greetingPrefix(lang string) (prefix string) {
	switch lang {
	case telugu:
		prefix = namastePrefix
	case kannada:
		prefix = namaskaraPrefix
	default:
		prefix = helloPrefix
	}
	return
}

func Greet(name, lang string) string {
	if name == "" {
		name = "World"
	}

	return greetingPrefix(lang) + name + exclamation
}

func main() {
	fmt.Println(Greet("Alia", ""))
}
