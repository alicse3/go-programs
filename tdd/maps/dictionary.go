package dictionary

var (
	ErrNotFound          = DictionaryErr("Word doesn't exist")
	ErrWordAlreadyExists = DictionaryErr("Word already exist")
	ErrWordDoestExists   = DictionaryErr("Can't update because word doesn't exist")
)

type Dictionary map[string]string

type DictionaryErr string

func (d DictionaryErr) Error() string {
	return string(d)
}

func (d Dictionary) Search(word string) (string, error) {
	desc, ok := d[word]

	if !ok {
		return "", ErrNotFound
	}

	return desc, nil
}

func (d Dictionary) Add(word, desc string) error {
	_, err := d.Search(word)

	switch err {
	case ErrNotFound:
		d[word] = desc
	case nil:
		return ErrWordAlreadyExists
	default:
		return err
	}
	return nil
}

func (d Dictionary) Update(word, desc string) error {
	_, err := d.Search(word)

	switch err {
	case ErrNotFound:
		return ErrWordDoestExists
	case nil:
		d[word] = desc
	default:
		return err
	}
	return nil
}

func (d Dictionary) Delete(word string) {
	delete(d, word)
}
