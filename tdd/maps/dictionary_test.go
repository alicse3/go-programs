package dictionary

import (
	"testing"
)

func TestSearch(t *testing.T) {
	t.Run("existing word search", func(t *testing.T) {
		dictionary := Dictionary{"ali": "yes"}

		got, _ := dictionary.Search("ali")
		want := "yes"

		assertStrings(t, got, want)
	})

	t.Run("non-existing word search", func(t *testing.T) {
		dictionary := Dictionary{}

		_, err := dictionary.Search("ali")

		if err == nil {
			t.Fatal("ok")
		}

		assertError(t, err, ErrNotFound)
	})
}

func TestAdd(t *testing.T) {
	t.Run("add new word", func(t *testing.T) {
		dictionary := Dictionary{}
		dictionary.Add("Uk", "Wow!")

		assertDescription(t, dictionary, "Uk", "Wow!")
	})

	t.Run("add existing word", func(t *testing.T) {
		dictionary := Dictionary{"Uk": "Wow!"}
		err := dictionary.Add("Uk", "Meow...")

		assertError(t, err, ErrWordAlreadyExists)
		assertDescription(t, dictionary, "Uk", "Wow!")
	})
}

func TestUpdate(t *testing.T) {
	t.Run("update existing word", func(t *testing.T) {
		dictionary := Dictionary{"Me": "You"}
		dictionary.Update("Me", "Meme...")

		assertDescription(t, dictionary, "Me", "Meme...")
	})

	t.Run("update non-existing word", func(t *testing.T) {
		dictionary := Dictionary{}

		err := dictionary.Update("Empty", "Yes!")

		assertError(t, err, ErrWordDoestExists)
	})
}

func TestDelete(t *testing.T) {
	t.Run("delete existing word", func(t *testing.T) {
		dictionary := Dictionary{"What": "Yes, delete me..."}

		dictionary.Delete("What")
		_, err := dictionary.Search("What")
		if err != ErrNotFound {
			t.Errorf("got %q but want %q", err, ErrNotFound)
		}
	})
}

func assertDescription(t *testing.T, dictionary Dictionary, word, description string) {
	t.Helper()

	got, err := dictionary.Search(word)

	if err != nil {
		t.Fatal("word should be searchable", err)
	}

	if got != description {
		t.Errorf("got %s but want %s", got, description)
	}
}

func assertError(t *testing.T, got, want error) {
	t.Helper()
	if got != want {
		t.Errorf("got %q but want %q", got, want)
	}
}

func assertStrings(t *testing.T, got, want string) {
	t.Helper()
	if got != want {
		t.Errorf("got %q but want %q", got, want)
	}
}
