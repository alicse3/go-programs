package checker

type WebsiteChecker func(string) bool

type result struct {
	string
	bool
}

func WebsiteCheck(wc WebsiteChecker, urls []string) map[string]bool {
	status := make(map[string]bool, len(urls))

	resultChannel := make(chan result, len(urls))

	for _, url := range urls {
		url := url
		go func() {
			resultChannel <- result{
				url,
				wc(url),
			}
		}()
	}

	for i := 0; i < len(urls); i++ {
		result := <-resultChannel
		status[result.string] = result.bool
	}

	return status
}
