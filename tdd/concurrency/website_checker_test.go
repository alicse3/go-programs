package checker

import (
	"reflect"
	"testing"
	"time"
)

func TestWebsiteCheck(t *testing.T) {
	urls := []string{
		"www.google.com",
		"www.youtube.com",
		"www.bitbucket.org",
		"www.nothing.com",
	}

	checkerFunc := func(url string) bool {
		if url == "www.nothing.com" {
			return false
		}
		return true
	}

	got := WebsiteCheck(checkerFunc, urls)
	want := map[string]bool{
		"www.google.com":    true,
		"www.youtube.com":   true,
		"www.bitbucket.org": true,
		"www.nothing.com":   false,
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v but want %v", got, want)
	}
}

func BenchmarkWebsiteCheck(b *testing.B) {
	urls := make([]string, 100)

	for i := 0; i < 100; i++ {
		urls[i] = "something"
	}

	// Accept string type argument but ignore it's usage
	slowCheckFunc := func(_ string) bool {
		time.Sleep(10 * time.Millisecond)
		return true
	}

	for i := 0; i < b.N; i++ {
		WebsiteCheck(slowCheckFunc, urls)
	}
}
