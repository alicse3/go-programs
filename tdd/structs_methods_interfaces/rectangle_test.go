package rectangle

import "testing"

func TestPerimeter(t *testing.T) {
	rectangle := Rectangle{10.00, 20.00}
	got := Perimeter(rectangle)
	want := 60.00

	if got != want {
		t.Errorf("got %f but want %f", got, want)
	}
}

func TestArea(t *testing.T) {
	checkArea := func(t *testing.T, shape Shape, want float64) {
		t.Helper()
		got := shape.Area()

		if got != want {
			t.Errorf("got %f but want %f", got, want)
		}
	}

	t.Run("should return rectangle area", func(t *testing.T) {
		rectangle := Rectangle{10.00, 20.00}
		checkArea(t, rectangle, 200.00)
	})

	t.Run("should return circle area", func(t *testing.T) {
		circle := Circle{10}
		checkArea(t, circle, 314.1592653589793)
	})
}

func TestTableArea(t *testing.T) {
	testAreas := []struct {
		name  string
		shape Shape
		want  float64
	}{
		{name: "Rectangle", shape: Rectangle{Width: 100.00, Height: 200.50}, want: 20050.00},
		{name: "Circle", shape: Circle{Radius: 10.00}, want: 314.1592653589793},
		{name: "Triangle", shape: Triangle{Base: 12, Height: 6}, want: 36.00},
	}

	for _, tt := range testAreas {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.shape.Area()
			if got != tt.want {
				t.Errorf("%#v: got %f but want %f", tt.shape, got, tt.want)
			}
		})
	}
}
