package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

func Greet(writer io.Writer, name string) {
	fmt.Fprintf(writer, "Hello, %s", name)
}

func GreetHandler(w http.ResponseWriter, r *http.Request) {
	Greet(w, "world!")
}

func main() {
	Greet(os.Stdout, "Puppy")
	http.ListenAndServe(":8000", http.HandlerFunc(GreetHandler))
}
