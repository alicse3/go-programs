package counter

import (
	"sync"
	"testing"
)

func TestCounter(t *testing.T) {
	t.Run("consecutive counter test", func(t *testing.T) {
		counter := Counter{}
		counter.Inc()
		counter.Inc()
		counter.Inc()

		assertCounter(t, counter.Value(), 3)
	})

	t.Run("concurrent counter test", func(t *testing.T) {
		expectedCount := 1000

		counter := Counter{}

		var wg = sync.WaitGroup{}
		for i := 0; i < expectedCount; i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()

				counter.Inc()
			}()
		}
		wg.Wait()

		assertCounter(t, counter.Value(), expectedCount)
	})
}

func assertCounter(t *testing.T, got, want int) {
	t.Helper()

	if got != want {
		t.Errorf("got '%d' but want '%d'", got, want)
	}
}
