package integers

import (
	"fmt"
	"testing"
)

func TestAdd(t *testing.T) {
	got := Add(2, 3)
	want := 5

	if got != want {
		t.Errorf("got %d but want %d", got, want)
	}
}

func ExampleAdd() {
	sum := Add(10, 20)
	fmt.Println(sum)
	// Output: 30
}
