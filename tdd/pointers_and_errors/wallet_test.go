package wallet

import "testing"

func TestWaller(t *testing.T) {

	t.Run("should deposit amount", func(t *testing.T) {
		wallet := Wallet{}

		wallet.Deposit(Bitcoin(10))

		assertBalance(t, wallet, Bitcoin(10))
	})

	t.Run("withdraw with insufficient funds", func(t *testing.T) {
		wallet := Wallet{Bitcoin(100)}

		err := wallet.Withdraw(Bitcoin(1000))

		assertBalance(t, wallet, Bitcoin(100))
		assertError(t, err, ErrorInsufficientFunds)
	})

	t.Run("withdraw with funds available", func(t *testing.T) {
		wallet := Wallet{Bitcoin(100)}

		err := wallet.Withdraw(Bitcoin(70))

		assertBalance(t, wallet, Bitcoin(30))
		assertNoError(t, err)
	})
}

func assertBalance(t *testing.T, wallet Wallet, want Bitcoin) {
	t.Helper()

	got := wallet.Balance()

	if got != want {
		t.Errorf("got %q but want %q", got, want)
	}
}

func assertError(t *testing.T, got, want error) {
	t.Helper()

	if got == nil {
		t.Fatal("funds available")
	}

	if got == want {
		t.Error(want)
	}
}

func assertNoError(t *testing.T, err error) {
	if err != nil {
		t.Fatal("withdraw error")
	}
}
