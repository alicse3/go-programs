package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	root := context.Background()
	ctx, cancel := context.WithTimeout(root, time.Second)
	defer cancel()

	waitAndPrint(ctx, 3*time.Second, "Hello World!")
}

func waitAndPrint(ctx context.Context, d time.Duration, message string) {
	select {
	case <-time.After(d):
		fmt.Println(message)
	case <-ctx.Done():
		fmt.Print(ctx.Err())
	}
}
