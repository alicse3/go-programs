package main

import (
	"context"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

const URL = "http://localhost:8000"

func main() {
	// onTimeout()

	onCancel()
}

func onTimeout() {
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, time.Second)
	defer cancel()

	req, err := http.NewRequest(http.MethodGet, URL, nil)
	checkError(err)
	req = req.WithContext(ctx)

	res, err := http.DefaultClient.Do(req)
	checkError(err)
	defer res.Body.Close()

	io.Copy(os.Stdout, res.Body)
}

func onCancel() {
	req, err := http.NewRequest(http.MethodGet, URL, nil)
	checkError(err)

	client := http.Client{}
	resp, err := client.Do(req)
	checkError(err)
	defer resp.Body.Close()

	io.Copy(os.Stdout, resp.Body)
}

func checkError(err error) {
	if err != nil {
		log.Println(err)
	}
}
