package poker

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestGETPlayers(t *testing.T) {
	server := NewPlayerServer(
		&StubPlayerStore{
			scores: map[string]int{
				"kitty": 3,
				"puppy": 5,
			},
		})

	t.Run("should return kitty's score", func(t *testing.T) {
		req := newGetScoreRequest("kitty")
		res := httptest.NewRecorder()

		server.ServeHTTP(res, req)

		assertStatusCode(t, res.Code, http.StatusOK)
		assertResponseBody(t, res.Body.String(), "3")
	})

	t.Run("should return puppy's score", func(t *testing.T) {
		req := newGetScoreRequest("puppy")
		res := httptest.NewRecorder()

		server.ServeHTTP(res, req)

		assertStatusCode(t, res.Code, http.StatusOK)
		assertResponseBody(t, res.Body.String(), "5")
	})

	t.Run("should return not found error", func(t *testing.T) {
		req := newGetScoreRequest("me")
		res := httptest.NewRecorder()

		server.ServeHTTP(res, req)

		assertStatusCode(t, res.Code, http.StatusNotFound)
	})
}

func TestPOSTPlayers(t *testing.T) {
	server := NewPlayerServer(
		&StubPlayerStore{
			scores: map[string]int{},
		},
	)

	t.Run("should return accepted on POST", func(t *testing.T) {
		req := newPostScoreRequest("kitty")
		res := httptest.NewRecorder()

		server.ServeHTTP(res, req)

		assertStatusCode(t, res.Code, http.StatusAccepted)
	})

	t.Run("should record wins", func(t *testing.T) {
		stubStore := &StubPlayerStore{
			scores: map[string]int{},
		}
		server := NewPlayerServer(stubStore)

		req := newPostScoreRequest("kitty")
		res := httptest.NewRecorder()

		server.ServeHTTP(res, req)

		assertStatusCode(t, res.Code, http.StatusAccepted)

		if len(stubStore.winCalls) != 1 {
			t.Errorf("got %d but want %d", len(stubStore.winCalls), 1)
		}

		if stubStore.winCalls[0] != "kitty" {
			t.Errorf("expected player is not %q", stubStore.winCalls[0])
		}
	})
}

func TestLeague(t *testing.T) {
	store := &StubPlayerStore{}
	server := NewPlayerServer(store)

	t.Run("should return 200 on /league", func(t *testing.T) {
		req, _ := http.NewRequest(http.MethodGet, "/league", nil)
		res := httptest.NewRecorder()

		server.ServeHTTP(res, req)

		var got League

		err := json.NewDecoder(res.Body).Decode(&got)
		if err != nil {
			t.Fatalf("Unable to decode %q response body as Player json '%v'", res.Body, err)
		}

		assertStatusCode(t, res.Code, http.StatusOK)
	})

	t.Run("should return league table a json", func(t *testing.T) {
		expectedLeagues := League{
			{
				Name: "puppy",
				Wins: 3,
			}, {
				Name: "kitty",
				Wins: 2,
			}, {
				Name: "uk",
				Wins: 5,
			},
		}

		req := newLeagueRequest()
		res := httptest.NewRecorder()

		store := &StubPlayerStore{
			scores:   nil,
			winCalls: nil,
			league:   expectedLeagues,
		}
		server := NewPlayerServer(store)

		server.ServeHTTP(res, req)

		assertStatusCode(t, res.Code, http.StatusOK)
		assertLeague(t, expectedLeagues, getLeaguesFromResponse(t, res.Body))
		assertContentType(t, res.Result().Header.Get("content-type"), jsonContentType)
	})
}

func assertContentType(t *testing.T, got, want string) {
	t.Helper()

	if got != want {
		t.Errorf("got %q but want %q", got, want)
	}
}

func getLeaguesFromResponse(t *testing.T, res io.Reader) League {
	t.Helper()

	var leagues League

	err := json.NewDecoder(res).Decode(&leagues)
	if err != nil {
		t.Fatalf("error in decoding as json, %q", err)
	}

	return leagues
}

func assertLeague(t *testing.T, got, want League) {
	t.Helper()

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v but want %v", got, want)
	}
}

func newLeagueRequest() *http.Request {
	req, _ := http.NewRequest(http.MethodGet, "/league", nil)
	return req
}

func newPostScoreRequest(player string) *http.Request {
	req, _ := http.NewRequest(http.MethodPost, fmt.Sprintf("/players/%s", player), nil)
	return req
}

func newGetScoreRequest(player string) *http.Request {
	req, _ := http.NewRequest(http.MethodGet, fmt.Sprintf("/players/%s", player), nil)
	return req
}

func assertResponseBody(t *testing.T, got, want string) {
	t.Helper()

	if got != want {
		t.Errorf("got %q but want %q", got, want)
	}
}

func assertStatusCode(t *testing.T, got, want int) {
	t.Helper()

	if got != want {
		t.Errorf("got %d but want %d", got, want)
	}
}
