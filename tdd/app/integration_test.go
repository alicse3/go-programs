package poker

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRecordingWinsAndRetrievingThem(t *testing.T) {
	database, cleanDatabase := createTempFile(t, "[]")
	defer cleanDatabase()
	fileSystemStore, err := NewFileSystemPlayerStore(database)
	assertNoError(t, err)

	server := NewPlayerServer(fileSystemStore)
	player := "puppy"

	server.ServeHTTP(httptest.NewRecorder(), newPostScoreRequest(player))
	server.ServeHTTP(httptest.NewRecorder(), newPostScoreRequest(player))
	server.ServeHTTP(httptest.NewRecorder(), newPostScoreRequest(player))

	t.Run("get score", func(t *testing.T) {
		res := httptest.NewRecorder()
		server.ServeHTTP(res, newGetScoreRequest(player))

		assertStatusCode(t, res.Code, http.StatusOK)
		assertResponseBody(t, res.Body.String(), "3")
	})

	t.Run("get league", func(t *testing.T) {
		res := httptest.NewRecorder()
		server.ServeHTTP(res, newLeagueRequest())
		assertStatusCode(t, res.Code, http.StatusOK)

		got := getLeaguesFromResponse(t, res.Body)
		want := League{
			{
				"puppy", 3,
			},
		}
		assertLeague(t, got, want)
	})
}
