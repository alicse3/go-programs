package poker

import (
	"encoding/json"
	"fmt"
	"io"
)

type League []Player

func (l League) Find(name string) *Player {
	for index, player := range l {
		if player.Name == name {
			return &l[index]
		}
	}
	return nil
}

func NewLeague(reader io.Reader) (League, error) {
	var leagues League

	err := json.NewDecoder(reader).Decode(&leagues)
	if err != nil {
		err = fmt.Errorf("unable to decode as player, %q", err)
	}

	return leagues, err
}
