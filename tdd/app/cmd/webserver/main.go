package main

import (
	"log"
	"net/http"

	poker "bitbucket.org/alicse3/go-programs/tdd/app"
)

const dbFileName = "game.db.json"

func main() {
	store, closeDBConn, err := poker.FileSystemPlayerStoreFromFile(dbFileName)
	if err != nil {
		log.Fatal(err)
	}
	defer closeDBConn()

	server := poker.NewPlayerServer(store)

	log.Fatal(http.ListenAndServe(":8000", server))
}
