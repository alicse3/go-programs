package poker

import (
	"io/ioutil"
	"os"
	"testing"
)

func TestFileSystemStore(t *testing.T) {
	t.Run("/league from a reader", func(t *testing.T) {
		database, cleanDatabase := createTempFile(t, `[
			{"Name": "puppy", "Wins": 1}
		]`)
		defer cleanDatabase()

		store, err := NewFileSystemPlayerStore(database)
		assertNoError(t, err)

		got := store.GetLeague()
		want := League{
			{Name: "puppy", Wins: 1},
		}

		assertLeague(t, got, want)

		got = store.GetLeague()
		assertLeague(t, got, want)
	})

	t.Run("get player score", func(t *testing.T) {
		database, cleanDatabase := createTempFile(t, `[
			{"Name": "puppy", "Wins": 10}
		]`)
		defer cleanDatabase()

		store, err := NewFileSystemPlayerStore(database)
		assertNoError(t, err)

		assertScoreEquals(t, store.GetPlayerScore("puppy"), 10)
	})

	t.Run("store wins for existing players", func(t *testing.T) {
		database, cleanDatabase := createTempFile(t, `[
			{"Name": "puppy", "Wins": 7},
			{"Name": "kitty", "Wins": 6}
		]`)
		defer cleanDatabase()

		player := "puppy"

		store, err := NewFileSystemPlayerStore(database)
		assertNoError(t, err)

		store.RecordWin(player)
		store.RecordWin(player)
		store.RecordWin(player)

		assertScoreEquals(t, store.GetPlayerScore(player), 10)
	})

	t.Run("store wins for new players", func(t *testing.T) {
		database, cleanDatabase := createTempFile(t, `[
			{"Name": "alice", "Wins": 100},
			{"Name": "bob", "Wins": 100}
		]`)
		defer cleanDatabase()

		player := "chi"

		store, err := NewFileSystemPlayerStore(database)
		assertNoError(t, err)

		store.RecordWin(player)

		assertScoreEquals(t, store.GetPlayerScore(player), 1)
	})

	t.Run("works with an empty file", func(t *testing.T) {
		file, clean := createTempFile(t, "")
		defer clean()

		_, err := NewFileSystemPlayerStore(file)
		assertNoError(t, err)
	})

	t.Run("should return leagues in sorted order by wins", func(t *testing.T) {
		file, clean := createTempFile(t, `[
			{"Name": "puppy", "Wins": 10},
			{"Name": "kitty", "Wins": 33}
		]`)
		defer clean()

		store, err := NewFileSystemPlayerStore(file)
		assertNoError(t, err)

		got := store.GetLeague()
		want := League{
			{Name: "kitty", Wins: 33},
			{Name: "puppy", Wins: 10},
		}

		assertLeague(t, got, want)
	})
}

func createTempFile(t *testing.T, data string) (*os.File, func()) {
	t.Helper()

	tmpFile, err := ioutil.TempFile("", "db")
	if err != nil {
		t.Fatalf("can't create a temp file, %q", err)
	}

	tmpFile.Write([]byte(data))

	removeFile := func() {
		tmpFile.Close()
		os.Remove(tmpFile.Name())
	}

	return tmpFile, removeFile
}

func assertScoreEquals(t *testing.T, got, want int) {
	t.Helper()

	if got != want {
		t.Errorf("got %d but want %d", got, want)
	}
}

func assertNoError(t *testing.T, err error) {
	t.Helper()

	if err != nil {
		t.Fatalf("got an error, %q", err)
	}
}
