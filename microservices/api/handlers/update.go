package handlers

import (
	"net/http"
	"strconv"

	"bitbucket.org/alicse3/go-programs/microservices/api/data"
	"github.com/gorilla/mux"
)

func (p *Products) UpdateProducts(rw http.ResponseWriter, r *http.Request) {
	p.l.Println("put products handler is called")

	routeVars := mux.Vars(r)
	id := routeVars["id"]
	productID, err := strconv.Atoi(id)
	if err != nil {
		http.Error(rw, "invalid product id", http.StatusBadRequest)
	}

	prod := r.Context().Value(ProductKey{}).(data.Product)

	err = data.PutProducts(prod, productID)
	if err == data.ErrProductNotFound {
		http.Error(rw, "invalid product request", http.StatusBadRequest)
		return
	}

	if err != nil {
		http.Error(rw, "invalid request", http.StatusBadRequest)
		return
	}
}
