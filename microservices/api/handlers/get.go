package handlers

import (
	"net/http"

	"bitbucket.org/alicse3/go-programs/microservices/api/data"
)

func (p *Products) GetProducts(rw http.ResponseWriter, r *http.Request) {
	p.l.Println("get products handler is called")

	products := data.GetProducts()
	err := products.ToJSON(rw)
	if err != nil {
		http.Error(rw, "unable to marshal products as json", http.StatusInternalServerError)
	}
}
