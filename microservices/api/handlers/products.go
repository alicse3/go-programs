package handlers

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/alicse3/go-programs/microservices/api/data"
)

type Products struct {
	l *log.Logger
}

func NewProducts(l *log.Logger) *Products {
	return &Products{l}
}

type ProductKey struct{}

func (p *Products) MiddlewareProductValidation(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		product := data.Product{}

		err := product.FromJSON(r.Body)
		if err != nil {
			p.l.Println("[ERROR] deserializing product : ", err)
			http.Error(rw, "unable to unmarshal json", http.StatusBadRequest)
			return
		}

		err = product.Validate()
		if err != nil {
			p.l.Println("[ERROR] validating product : ", err)
			http.Error(rw, fmt.Sprintf("field validation error : %s", err), http.StatusBadRequest)
			return
		}

		// Add a product to the context
		// Get a context from request r and create a new context with value
		ctx := context.WithValue(r.Context(), ProductKey{}, product)

		// Create a new request with new context
		req := r.WithContext(ctx)

		next.ServeHTTP(rw, req)
	})
}
