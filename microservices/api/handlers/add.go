package handlers

import (
	"net/http"

	"bitbucket.org/alicse3/go-programs/microservices/api/data"
)

func (p *Products) AddProducts(rw http.ResponseWriter, r *http.Request) {
	p.l.Println("post products handler is called")

	prod := r.Context().Value(ProductKey{}).(data.Product)

	data.PostProducts(prod)
}
