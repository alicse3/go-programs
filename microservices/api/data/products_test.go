package data

import "testing"

func TestStructValidation(t *testing.T) {
	product := Product{
		Name:  "Tea",
		Price: 1.00,
		SKU:   "abc-abcd-abcde",
	}

	err := product.Validate()
	if err != nil {
		t.Fatal(err)
	}
}
