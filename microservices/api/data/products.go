package data

import (
	"encoding/json"
	"errors"
	"io"
	"regexp"
	"time"

	"github.com/go-playground/validator"
)

type Product struct {
	ID          int     `json:"id"`
	Name        string  `json:"name" validate:"required"`
	Description string  `json:"description"`
	Price       float32 `json:"price" validate:"gt=0"`
	SKU         string  `json:"sku" validate:"required,sku"`
	CreatedOn   string  `json:"-"` // - means, ignore it in output completely
	UpdatedOn   string  `json:"-"`
	DeletedOn   string  `json:"-"`
}

type Products []*Product

func (p *Products) ToJSON(w io.Writer) error {
	encode := json.NewEncoder(w)
	return encode.Encode(p)
}

func (p *Product) FromJSON(r io.Reader) error {
	decode := json.NewDecoder(r)
	return decode.Decode(p)
}

func (p *Product) Validate() error {
	validate := validator.New()
	validate.RegisterValidation("sku", validateSKU)

	return validate.Struct(p)
}

func validateSKU(fl validator.FieldLevel) bool {
	reg := regexp.MustCompile(`[a-z]+-[a-z]+-[a-z]`)
	allMatches := reg.FindAllString(fl.Field().String(), -1)

	if len(allMatches) != 1 {
		return false
	}

	return true
}

func GetProducts() Products {
	return productList
}

func PostProducts(product Product) {
	product.ID = getNextID()
	productList = append(productList, &product)
}

func PutProducts(product Product, productID int) error {
	_, index, err := findProduct(productID)

	if index != -1 {
		product.ID = productID
		productList[index] = &product
	}

	return err
}

var ErrProductNotFound = errors.New("Product not found")

func findProduct(pid int) (*Product, int, error) {
	for index, product := range productList {
		if product.ID == pid {
			return product, index, nil
		}
	}

	return nil, -1, ErrProductNotFound
}

func getNextID() int {
	return productList[len(productList)-1].ID + 1
}

var productList = []*Product{
	{
		ID:          1,
		Name:        "Latte",
		Description: "Some latte",
		Price:       2.45,
		SKU:         "acb323",
		CreatedOn:   time.Now().UTC().String(),
		UpdatedOn:   time.Now().UTC().String(),
	},
	{
		ID:          2,
		Name:        "Espresso",
		Description: "Some espresso",
		Price:       1.99,
		SKU:         "fjd34",
		CreatedOn:   time.Now().UTC().String(),
		UpdatedOn:   time.Now().UTC().String(),
	},
}
