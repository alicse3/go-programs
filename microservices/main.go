package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/alicse3/go-programs/microservices/api/handlers"
	"github.com/gorilla/mux"
)

func main() {
	logger := log.New(os.Stdout, "[some service] ", log.LstdFlags)

	// helloHandler := handlers.NewHello(logger)
	productsHandler := handlers.NewProducts(logger)

	serverMux := mux.NewRouter()

	getRouter := serverMux.Methods(http.MethodGet).Subrouter()
	getRouter.HandleFunc("/products", productsHandler.GetProducts)

	postRouter := serverMux.Methods(http.MethodPost).Subrouter()
	postRouter.HandleFunc("/products", productsHandler.AddProducts)
	postRouter.Use(productsHandler.MiddlewareProductValidation)

	putRouter := serverMux.Methods(http.MethodPut).Subrouter()
	putRouter.HandleFunc("/products/{id:[0-9]+}", productsHandler.UpdateProducts)
	putRouter.Use(productsHandler.MiddlewareProductValidation)

	server := http.Server{
		Addr:         ":8000",
		Handler:      serverMux,
		IdleTimeout:  120 * time.Second,
		ReadTimeout:  1 * time.Second,
		WriteTimeout: 1 * time.Second,
	}

	go func() {
		if err := server.ListenAndServe(); err != nil {
			logger.Fatal(err)
		}
	}()

	signalChannel := make(chan os.Signal)
	signal.Notify(signalChannel, os.Interrupt)
	signal.Notify(signalChannel, os.Kill)

	oSignal := <-signalChannel
	logger.Println("shutting down gracefully : ", oSignal)

	// If handlers are working, after 30 seconds gracefully close it
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		logger.Println(err)
	}
}
