package main

import (
	"sync"
	"time"
)

func fib(n int) chan int {
	out := make(chan int)

	go func() {
		defer close(out)

		for i, j := 0, 1; i <= n; i, j = i+j, i {
			out <- i
		}
	}()

	return out
}

func main() {
	// for value := range fib(100000000000000) {
	// 	fmt.Println(value)
	// }

	publisher := eventSubject{
		observers: sync.Map{},
	}

	t := time.Now()

	observer := eventObserver{
		id:   0,
		time: t,
	}

	publisher.AddListener(observer)
	for i := 1; i <= 3; i++ {
		publisher.AddListener(eventObserver{
			id:   i,
			time: t,
		})
	}

	go func() {
		select {
		case <-time.After(1 * time.Millisecond):
			for i := 1; i <= 3; i++ {
				publisher.RemoveListener(eventObserver{
					id:   i,
					time: t,
				})
			}
		}
	}()

	for value := range fib(1000000000000000000) {
		publisher.Notify(Event{data: value})
	}
}
