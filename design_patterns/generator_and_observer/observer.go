package main

import (
	"fmt"
	"sync"
	"time"
)

type (
	Event struct {
		data int
	}

	Observer interface {
		NotifyCallback(Event)
	}

	Subject interface {
		AddListener(Observer)
		RemoveListener(Observer)
		Notify(Event)
	}

	eventObserver struct {
		id   int
		time time.Time
	}

	eventSubject struct {
		observers sync.Map
	}
)

func (e eventObserver) NotifyCallback(event Event) {
	fmt.Printf("Observer %d received %d after %v\n", e.id, event.data, time.Since(e.time))
}

func (e *eventSubject) AddListener(o Observer) {
	e.observers.Store(o, struct{}{})
}

func (e *eventSubject) RemoveListener(o Observer) {
	e.observers.Delete(o)
}

func (e *eventSubject) Notify(event Event) {
	e.observers.Range(func(key, value interface{}) bool {
		if key == nil || value == nil {
			return false
		}

		key.(Observer).NotifyCallback(event)
		return true
	})
}
