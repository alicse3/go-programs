package main

// To include more than one package
import (
	"fmt"
	"math"
)

func main() {
	name := "Ali"
	fmt.Println("Hello ", name, "! Welcome to Go World!")
	fmt.Println("Sqrt of 16 is : ", math.Sqrt(16))
	fmt.Println("Floor of 3.3 is : ", math.Floor(3.3))
	fmt.Println("Ceil of 3.3 is : ", math.Ceil(3.3))
}
