package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

// Data sender to a channel
func greet(greetList chan string, name string) {
	defer wg.Done()
	greetList <- "Welcome " + name + "!"
}

func main() {
	// Data to send
	names := []string{
		"Ali",
		"Puppy",
		"Kitty",
		"Teddy",
		"Gopher",
		"UK",
	}

	// Create channel
	greetingChannel := make(chan string, len(names))

	// Send data to channel
	for i := 0; i < len(names); i++ {
		wg.Add(1)
		go greet(greetingChannel, names[i])
	}

	// Wait for all go routines to complete its execution before closing a channel
	wg.Wait()

	// Close channel
	close(greetingChannel)

	// Receive data
	for greeting := range greetingChannel {
		fmt.Println(greeting)
	}
}
