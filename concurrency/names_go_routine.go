package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

// Recover function
func rcvr() {
	if r := recover(); r != nil {
		fmt.Println("Recovered from panic :", r)
	}
}

// Defer statements runs First In Last Out order
func sayHi(name string) {
	defer wg.Done() // Runs 2nd
	defer rcvr()    // Runs 1st
	for i := 0; i < 5; i++ {
		fmt.Println("Hi " + name + "!")
		if i == 3 {
			panic("Omg! i is '3'")
		}
	}
}

func main() {
	wg.Add(1)
	go sayHi("Ali")

	wg.Add(1)
	go sayHi("Puppy")

	wg.Add(1)
	go sayHi("UK")

	wg.Wait()
}
