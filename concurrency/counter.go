package main

import (
	"fmt"
	"sync"
)

var count int

var wg sync.WaitGroup

var mutex sync.Mutex

func counter() {
	defer wg.Done()

	mutex.Lock()
	count++
	mutex.Unlock()
}

func main() {
	for i := 0; i < 100; i++ {
		wg.Add(1)
		go counter()
	}
	wg.Wait()

	fmt.Println("Count is :", count)
}
