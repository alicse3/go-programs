package main

import (
	"fmt"
	"log"
	"net/http"
	"sync"
)

var wg sync.WaitGroup

var URLs = []string{
	"https://www.hackerrank.com/",
	"https://www.youtube.com/",
	"https://www.google.com",
	"https://www.hackerrank.com",
	"https://www.hackerearth.com",
}

func handler(w http.ResponseWriter, r *http.Request) {
	recoverPanic := func() {
		if r := recover(); r != nil {
			fmt.Fprintln(w, r)
		}
	}

	for _, url := range URLs {
		wg.Add(1)
		go func(u string) {
			defer wg.Done()
			defer recoverPanic()

			resp, err := http.Get(u)
			if err != nil {
				panic(err)
			}
			fmt.Fprintln(w, string(resp.Status))

		}(url)
	}
	wg.Wait()
}

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8000", nil))
}
