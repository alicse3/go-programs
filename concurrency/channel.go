package main

import "fmt"

func main() {
	rec := sender("Ali")
	for i := 0; i < 100; i++ {
		fmt.Println(<-rec)
	}
}

func sender(name string) <-chan string {
	names := make(chan string)

	go func() {
		for i := 0; ; i++ {
			names <- fmt.Sprintf("%s %d", name, i)
		}
	}()

	go func() {
		for i := 0; ; i++ {
			names <- fmt.Sprintf("%s %d", name, i*i*i*i*100)
		}
	}()

	return names
}
