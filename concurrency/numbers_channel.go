package main

import "fmt"

func main() {
	// -------------------------------
	// Create a channel
	numbers := make(chan int)
	defer close(numbers)

	// Create a func to send data to a channel
	send := func(number chan int) {
		number <- 3
	}

	// Make it as a go routine
	go send(numbers)

	// Receive a data from channel
	number := <-numbers
	fmt.Println(number)

	// -------------------------------
	// Send some more data
	sendMoreData := func(nums chan int) {
		for i := 0; i < 10; i++ {
			if i%2 == 0 {
				nums <- i
			}
		}
	}
	go sendMoreData(numbers)

	// Receive some more data
	for i := 0; i < 5; i++ {
		evenNumber := <-numbers
		fmt.Println(evenNumber)
	}
	// -------------------------------
}
