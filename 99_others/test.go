package main

import (
	"fmt"
	"math"
	"reflect"
	"runtime"
	"strconv"
	"sync"
)

const (
	x = 10
	y = 20
	z = 30
)

type Person struct {
	name       string
	age        int
	otherNames []string
}

type Animal struct {
	name string
	age  int `required max:"10"` // Tags
}

type Bird struct {
	Animal
	speed  float32
	canFly bool
}

var wg = sync.WaitGroup{}

func main() {
	// var ch chan(int)
	// wg.Add(2)
	// go func ()  {
	// 	output := <- ch
	// 	fmt.Println(output)
	// 	wg.Done()
	// }()
	// go func ()  {
	// 	ch <- 42
	// 	wg.Done()
	// }()
	// wg.Wait()

	ref := reflect.TypeOf(Bird{})
	field, _ := ref.FieldByName("age")
	fmt.Println(field)
	fmt.Println(field.Tag)

	bird := Bird{
		Animal: Animal{name: "A Bird", age: 26},
		speed:  100,
		canFly: true,
	}
	fmt.Println(bird)
	fmt.Println(bird.name)

	ali := Person{
		name: "Ali",
		age:  26,
		otherNames: []string{
			"Sabba",
		},
	}
	fmt.Println(ali)

	uk := Person{
		"UK",
		25,
		[]string{
			"Beautiful",
		},
	}
	fmt.Println(uk)

	fmt.Printf("%v %T\n", x, x)
	fmt.Printf("%v %T\n", y, y)
	fmt.Printf("%v %T\n", z, z)

	fmt.Println("Hello Ali!")

	var i int
	i = 3
	fmt.Println(i)

	j := 100
	fmt.Println(j)

	var k int = int(j)
	fmt.Println(k)
	fmt.Printf("%v, %T", k, k)
	fmt.Println()

	var s string = "Ali"
	fmt.Println(s)

	t := strconv.Itoa(j)
	fmt.Printf("%v %T\n", t, t)

	v := true
	fmt.Printf("%v %T\n", v, v)

	var vr bool = false
	fmt.Printf("%v %T\n", vr, vr)

	var b = []byte(s)
	fmt.Println(b)

	const me string = "Ali S"
	fmt.Println(me)

	const something = "Nothing"
	fmt.Println(something)

	names := [3]string{"Ali", "Puppy", "UK"}
	fmt.Printf("%v\n", names)
	fmt.Println(names)

	nums := []int{1, 2, 3}
	fmt.Println(nums)

	flts := [...]float32{2, 3.4, 5.10}
	fmt.Println(flts)
	fmt.Println(len(flts))

	someData := []string{"Hello World!", "1", "3.3", "true", "A"}
	fmt.Println(someData[1:])

	mk := make([]int, 3, 30)
	fmt.Println(mk)
	fmt.Println(len(mk))
	fmt.Println(cap(mk))
	mk = append(mk, []int{10, 20, 30, 40, 50}...)
	fmt.Println(mk, len(mk), cap(mk))

	m := map[string]int{
		"Ali":   26,
		"Puppy": 3,
		"UK":    25,
	}
	fmt.Println(m)
	delete(m, "Puppy")
	fmt.Println(m)
	_, ok := m["Something"]
	fmt.Println(ok)
	if value, yes := m["UK"]; yes {
		fmt.Println(value)
	}

	if true {
		fmt.Println("Ali!")
	} else {
		fmt.Println("UK!")
	}

	fmt.Println(!true)

	switch i := 2 + 5; i {
	case 1, 3, 5, 7, 9:
		fmt.Println("Odd")
		fallthrough
	case 0, 2, 4, 6, 8:
		fmt.Println("Even")
		break
		fmt.Println("Hello There!")
	default:
		fmt.Println("Default")
	}

	var inter interface{} = 3
	switch inter {
	case inter:
		fmt.Println("Integer")
	default:
		fmt.Println("Other")
	}

	for i := 0; i < 5; i += 2 {
		fmt.Println(i)
	}

Outerloop:
	for i := 0; i < 5; i++ {
		for j := 0; j < 5; j++ {
			fmt.Print(i*j, " ")
			if i*j > 10 {
				break Outerloop
			}
		}
		fmt.Println()
	}

	fmt.Println()
	someValues := []string{"Zero", "One", "Two", "Three"}
	for key, val := range someValues {
		fmt.Println(key, val)
	}
	fmt.Println("============")

	// fmt.Println("One");
	// defer fmt.Println("Two");
	// fmt.Println("Three");

	// panic("Something went wrong!")

	var alice int = 10
	var bob *int = &alice
	fmt.Println(alice, *bob, bob)
	*bob = 30
	fmt.Println(alice, &alice, *bob, bob)

	greet("Ali!")
	saySomething("Hey! ", "What's Up?")
	sum(1, 2, 3, 4, 5)
	fmt.Println(getSum(1, 2, 3))
	res, err := devide(30, 0)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(res)

	nthng := nothing{
		name: "Ali",
		age:  26,
	}
	nthng.sayNothing()

	f := func() {
		fmt.Println("Hello There!")
	}
	f()

	var f2 func() = func() {
		fmt.Println("Hey, Yo!")
	}
	f2()

	var w Writer = ConsoleWriter{}
	w.Write([]byte("Hello Ali!"))

	var intf interface{} = "Ali"
	switch intf.(type) {
	case int:
		fmt.Println("int type")
	case string:
		fmt.Println("string type")
	default:
		fmt.Println("unknown type")
	}

	// go callMe()
	// time.Sleep(100 * time.Millisecond)
	wg.Add(1)
	go callMe()
	wg.Wait()

	fmt.Println("No. of threads : ", runtime.GOMAXPROCS(-1))

	var input int
	_, err1 := fmt.Scan(&input)
	if err1 != nil {
		fmt.Println("Error : ", err)
	} else {
		fmt.Println("Input : ", input)
	}

	length := len(strconv.Itoa(412023599))
	fmt.Println(length)
	power := math.Pow10(length)
	fmt.Println(power)

	var name = "Mahammad Ali"
	fmt.Println(name)
}

func callMe() {
	fmt.Println("Called me...")
	wg.Done()
}

type Writer interface {
	Write([]byte) (int, error)
}

type ConsoleWriter struct{}

func (cw ConsoleWriter) Write(data []byte) (int, error) {
	n, error := fmt.Println(string(data))
	return n, error
}

func (n nothing) sayNothing() {
	fmt.Println(n.name, n.age)
}

type nothing struct {
	name string
	age  int
}

func greet(s string) {
	fmt.Println("Hello ", s)
}

func saySomething(first, second string) {
	fmt.Println(first, second)
}

func devide(a, b int) (int, error) {
	if b == 0 {
		return 0, fmt.Errorf("'b' shouldn't be 0.")
	}
	return a / b, nil
}

func sum(values ...int) {
	result := 0
	for k := range values {
		result += values[k]
	}
	fmt.Println(result)
}

func getSum(values ...int) int {
	result := 0
	// _(key), v(value)
	for _, v := range values {
		result += v
	}
	return result
}
