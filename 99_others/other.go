package main

import (
	"errors"
	"fmt"
	"math"
)

func getSqrt(num float64) (float64, error) {
	if num < 0 {
		return 0, errors.New("Not defined for negative numbers")
	}
	return math.Sqrt(num), nil
}

func main() {
	a, b := 10, 30
	c := a * b
	fmt.Println("Multiplication of", a, "and", b, "is", c)
	if a > b {
		fmt.Println(a, "is greater than", b)
	} else if a < b {
		fmt.Println(a, "is less than", b)
	} else {
		fmt.Println(a, "and", b, "are equal")
	}

	var array [3]int
	fmt.Println("Length of an array is", len(array))
	fmt.Println("All elements are initialised with values", array)
	array[1] = 3
	fmt.Println("Middle array value is filled", array)

	// var names [3]string
	// names[0] = "Ali"
	// names[1] = "Puppy"
	// names[2] = "Don't know?"

	// Short-hand array declaration and initialization for above names
	names := [3]string{"Ali", "Puppy", "Don't know?"}
	fmt.Println(names)

	// Use Slices if the size is not known
	ages := []int{24, 23, 2019}
	fmt.Println(ages)
	fmt.Println(ages[:])
	fmt.Println(ages[2:])
	fmt.Println(ages[:3])
	fmt.Println(ages[1:3])

	// Will create a new one instead of modifying an existing so re-assigning it back
	ages = append(ages, 99)
	fmt.Println(ages)

	// Dictionaries
	contacts := make(map[string]int)
	contacts["Ali"] = 7702671449
	contacts["Puppy"] = 9876543210
	contacts["Don't know?"] = 1000000001
	fmt.Println(contacts)
	fmt.Println(contacts["Don't know?"])

	delete(contacts, "Don't know?")
	fmt.Println(contacts)

	// Only loop present in Go is for
	for i := 1; i <= 10; i++ {
		fmt.Println("3 *", i, "=", (3 * i))
	}

	i := 11
	for i <= 20 {
		fmt.Println("3 *", i, "=", (3 * i))
		i++
	}

	for i, j := 0, 5; i < 5; i, j = i+1, j-1 {
		fmt.Println(i, j)
	}

	years := [5]int{2016, 2017, 2018, 2019, 2020}
	for index, value := range years {
		fmt.Println("index is", index, "and value is", value)
	}

	nameAndAge := make(map[string]int)
	nameAndAge["Ali"] = 24
	nameAndAge["Puppy"] = 23
	for key, value := range nameAndAge {
		fmt.Println("The name is", key, "and the Age is", value)
	}

	ageAndName := map[int]string{
		24: "Ali",
		23: "Puppy",
	}
	fmt.Println(ageAndName)

	// Returning multiple values
	sqrt, err := getSqrt(-16)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(sqrt)
	}

	// Structs
	person1 := human{name: "Ali", age: 24}

	var person2 human
	person2.name = "Puppy"
	person2.age = 23

	fmt.Println(person1)
	fmt.Println(person2)
	fmt.Println(person1.name)

	// Pointers
	point := 3
	fmt.Println(point)
	fmt.Println(&point)  // Prints Address
	fmt.Println(*&point) // Prints actual value stored in that address
	increment(&point)
	fmt.Println(point)

	// Type conversion
	valueF := 30.33
	valueI := int(valueF)
	fmt.Println(valueI)

	isIt := true
	fmt.Println(isIt)

	thisIsAString := "Hello World!"
	convertedToBytes := []byte(thisIsAString)
	fmt.Println(thisIsAString, convertedToBytes)

	r := 'a' // var r rune = 'a'
	fmt.Println(r)

	const aVar = 33
	fmt.Println(aVar + 99)
	fmt.Printf("%v, %T\n", aVar, aVar)

	// Copying Arrays
	imArray := [...]int{1, 2, 3}
	imAlsoArray := imArray // Replace to see the difference, imAlsoArray := &imArray
	imAlsoArray[0] = 3
	imAlsoArray[1] = 3
	fmt.Println(imArray, "<=>", imAlsoArray)

	multipleParams("Ali", "Puppy")

	printIt("Ali", "Puppy", "Someone")

	printThisOneAlso("Hmm...", 1, 2, 3, 4, 5)

	// Anonymous function
	func() {
		fmt.Println("Hello from an Anonymous function...")
	}()
}

type human struct {
	name string
	age  int
}

func increment(num *int) {
	*num++
}

func multipleParams(first, second string) {
	fmt.Println("Hello", first, "and", second, "! How are you doing?")
}

// Var-args
func printIt(values ...string) {
	for i := 0; i < len(values); i++ {
		fmt.Printf("%v, %s\n", i, values[i])
	}

	for _, v := range values {
		fmt.Println(v)
	}
}

// Var-args with multiple types, must be the last parameter in function
func printThisOneAlso(str string, values ...int) {
	fmt.Println(str)
	for _, v := range values {
		fmt.Println(v)
	}
}
