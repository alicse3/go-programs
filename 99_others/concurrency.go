package main

import (
	"fmt"
	"time"
)

func con(str string) {
	for i := 0; i < 10; i++ {
		fmt.Println(i, str)
		time.Sleep(time.Millisecond * 1000)
	}
}

func main() {
	go con("Ali")
	con("Puppy")
}
