package main

import "fmt"

func main() {
	switch 8 {
	case 1, 5, 7, 9:
		fmt.Println("Some Odds")
		break
	case 2, 4, 6, 8:
		fmt.Println("Some Evens")
		break
	case 3:
		fmt.Println("Number 3")
		break
	default:
		fmt.Println("None is matching...")
	}

	switch i := 1 + 2; i {
	case 2, 3, 5, 7, 11:
		fmt.Println("Prime number")
		break
	case 4, 6, 8, 10, 12:
		fmt.Println("Even numbers")
		break
	default:
		fmt.Println("None is matching...")
	}

	a := 10
	switch {
	case a > 10:
		fmt.Println("Greater than 10")
	case a < 10:
		fmt.Println("Less than 10")
	default:
		fmt.Println("Equals 10")
	}

	b := 30
	switch {
	case b >= 30:
		fmt.Println("Greater than or Equals to 30")
		fallthrough
	case b <= 30: // Even with b < 30, this case will be executed
		fmt.Println("Less than or Equals to 30")
	default:
		fmt.Println("None is matching")
	}
}
