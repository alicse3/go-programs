package main

import "fmt"

func main() {
	// Defer
	// If we place defer keyword before all the statements then the executed will be done by LIFO(Last In First Out) order
	fmt.Println("First")
	defer fmt.Println("Deferred") // Will be executed after main function
	fmt.Println("Last but not actually")

	// Panic
	fmt.Println("One")
	panicker()
	fmt.Println("None")
}

func panicker() {
	fmt.Println("About to panic")
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("Recover error", err)
		}
	}()
	panic("Something happened")
	fmt.Println("It's not going to print")
}
