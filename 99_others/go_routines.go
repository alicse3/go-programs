package main

import (
	"fmt"
	"sync"
)

var wg = sync.WaitGroup{}

func main() {
	for i := 0; i < 10; i++ {
		wg.Add(2)
		go first()
		go second()
	}
	wg.Wait()
}

func first() {
	fmt.Println("First")
	wg.Done()
}

func second() {
	fmt.Println("Second")
	wg.Done()
}
